﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Jenzabar.CRM;
using Jenzabar.CRM.Deserializers;
using Jenzabar.CRM.Utility;
using TMS.DataObjects;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class AvailableTerms: TMSWorkerBase
    {
        private TermData[] tl;
        public TermData[] GetTerms()
        {

            GetTermList();
            return tl;
            //var strXML = string.Empty;
            //var common = new Jenzabar.ERP.Common();
            //common.GetTerms(CRMConstants.CRSSCH_PORTLET_NAME, ref strXML);
            //var tl =  (TermList)PlugIn.MapXMLToObject(strXML, new XmlSerializer(typeof(TermList)));
            //return MapToTermData(tl);
            //var xmlserializer = new XmlSerializer(ret.GetType());
            //using (var writer = new StringWriter())
            //{
            //    xmlserializer.Serialize(writer, ret);
            //    return writer.ToString();
            //}
        }

        private TermData[] MapToTermData(TermList tl)
        {
            var lst =  tl.Terms.Select(item => new TermData {termCode = item.Key.Replace(';', '_'), termDesc = item.Description}).ToArray();
            foreach (var t in lst.Where(t => t.termCode == tl.CurrentTermKey))
            {
                t.currentTermFlag = true;
            }
            return lst;

        }

        private void GetTermList()
        {
            var sb = new StringBuilder();
            sb.Append("	SELECT DISTINCT year_def.yr_cde AS YrCde, year_def.yr_sort_order AS YrSortOrder, term_def.trm_cde AS TrmCde,	");
            sb.Append("	                term_def.trm_sort_order AS TrmSortOrder,  ytt.yr_trm_desc AS YrTrmDesc, year_def.yr_desc AS YrDesc,	");
            sb.Append("	                term_def.trm_desc AS TrmDesc, TRM_BEGIN_DTE AS StartDate, TRM_END_DTE AS EndDate,	");
            sb.Append("	                CASE WHEN GETDATE() > TRM_BEGIN_DTE AND GETDATE() < TRM_END_DTE THEN 'True' ELSE 'False' END AS CurrentTerm	");
            sb.Append("	FROM (( year_def	");
            sb.Append("	                                INNER JOIN year_term_table AS ytt ON year_def.yr_cde=ytt.yr_cde)	");
            sb.Append("	                                INNER JOIN term_def ON term_def.trm_cde=ytt.trm_cde)	");
            sb.Append("	WHERE (( ytt.show_on_web IN ('B', 'E')))	");
            sb.Append("	ORDER BY year_def.yr_sort_order DESC, year_def.yr_cde DESC, term_def.trm_sort_order DESC, term_def.trm_cde DESC	");

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sb.ToString(), conn);
            var reader = command.ExecuteReader();
            var lst = new List<TermData>();

            while (reader.Read())
            {
                var t = new TermData();
                //ids.Add(((int)reader["TRANS_KEY_LINE_NUM"]).ToString());
                t.termCode = reader["YrCde"] + "_" + reader["TrmCde"];
                t.termDesc = reader["YrTrmDesc"].ToString();
                t.termStartDate = SafeConvertDateTime(reader["StartDate"].ToString());
                t.termEndDate = SafeConvertDateTime(reader["EndDate"].ToString());
                t.currentTermFlag = Convert.ToBoolean(reader["CurrentTerm"].ToString());
                lst.Add(t);

            }
            tl = lst.ToArray();

            //load the datatable with the SqlDataReader
            //DataTable dataTable = new DataTable();
            //dataTable.Load(reader);

            //convert the datatable to xml
            //StringWriter xmlWriter = new StringWriter();
            //dataTable.WriteXml(xmlWriter);

            //return the xml
            //EventLog.WriteEntry(appName, sql + "<br />" + xmlWriter, EventLogEntryType.Warning);


            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}