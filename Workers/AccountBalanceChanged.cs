﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class AccountBalanceChanged : TMSWorkerBase
    {

        public StudentDataList GetAccountBalanceChanged(DateTime startDate, DateTime endDate, string[] eligibleItemId, string[] ineligibleItemId)
        {
            var lst = new StudentDataList {StudentData = new List<StudentData>()};

            var listofstudents = new AccountBalance().GetStudentsWithBalanceChange(startDate, endDate, eligibleItemId, ineligibleItemId);// : new ValidateStudent().GetAllStudents().Select(x => x.HostID).ToArray();

                lst.StudentData.AddRange(from id in listofstudents
                             let stuinfo = new ValidateStudent().GetValidStudentFromEx(id)
                             where stuinfo != null
                             select new StudentData
                                        {
                                            FirstName = stuinfo.FirstName,
                                            LastName = stuinfo.LastName,
                                            ID = id
                                        });
            
            TMSErrorHandler.SetAccountBalanceChangedSuccess(ref lst);
            return lst;
        }

    }
}