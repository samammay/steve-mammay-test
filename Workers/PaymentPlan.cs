﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class PaymentPlan : TMSWorkerBase
    {
        private readonly xmlPaymentPlanPayment xppp = new xmlPaymentPlanPayment();

        public xmlPaymentPlanPayment PostPaymentPlanPayment(string StudentID, string LName, double PaymentAmount, DateTime? PaymentDate,
                             string Source, string Comment, string BillingItem, string Balance, string TmsPaymentId,
                             string ARCode, string Term, string SourceCode)
        {
            if (!DoesPaymentPlanExist(StudentID, Term))
            {
                return TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.PaymentPlanDoesNotExist,
                                                                    (int)Errors.PaymentPlanDoesNotExist);
            }
            var g = new Gateway();
            var ret = g.PostPayment(StudentID, LName, PaymentAmount, PaymentDate, Source, Comment, BillingItem,
                                    TmsPaymentId, ARCode, Term, SourceCode);

            xppp.Status = ret.Status;
            xppp.MessageNbr = ret.MessageNbr;
            xppp.MessageText = ret.MessageText;
            xppp.TransactionGroup = ret.TransactionGroup;
            xppp.TransGroupCreationDate = ret.TransGroupCreationDate;
            xppp.Cashier = ret.Cashier;

            xppp.ChargeId = ret.ChargeId;
            xppp.DepositId = ret.DepositId;
            xppp.PaymentId = ret.PaymentId;
            xppp.StudentId = ret.StudentId;

            xppp.CreditId = UpdatePaymentPlan(StudentID, Balance, Term).ToString();

            return xppp;
        }



        protected bool DoesPaymentPlanExist(string studentID, string Term)
        {
            var termcode = Term.Split('_');
            var sql = string.Format("SELECT * from TRANS_HIST WHERE GROUP_NUM = '999999' AND ID_NUM = '{0}' AND CHG_TRM_TRAN_HIST = '{1}' AND CHG_YR_TRAN_HIST = '{2}'", studentID, termcode[1], termcode[0]);


            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();
            var hostids = new List<string>();

            while (reader.Read())
            {
                hostids.Add(((int)reader["ID_NUM"]).ToString());
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return hostids.Count > 0;
        }

        protected int CreateNewPaymentPlan(string StudentID, string Balance, string ARCode, string SubsidiaryCode, string Term, string Comment, string SourceCode)
        {
            var cmt = !string.IsNullOrEmpty(Comment) ? Comment : "TMS Payment Plan";
            var termcode = Term.Split('_');
            var sql = "INSERT INTO [TRANS_HIST]([SOURCE_CDE],[GROUP_NUM],[TRANS_KEY_LINE_NUM],[TRANS_DTE],[TRANS_AMT],[TRANS_DESC],[FOLIO],[ACCT_CDE],[ENCUMB_GL_FLAG],[ENCUMB_GL_TRANS_ST],[ID_NUM],[SUBSID_CDE],[OFFSET_FLAG],[SUBSID_TRANS_STS],[DISCOUNT],[AR_CDE],[OI_ALLOCATION],[UNDEFINED_11_2_FLD],[WORK_PERCENT],[FOR_CUR_DISCOUNT],[EXCHANGE_RATE],[PAYMENT_PLAN_NUM],[USER_NAME],[JOB_NAME],[JOB_TIME],[CHG_TRM_TRAN_HIST],[CHG_YR_TRAN_HIST])";
            sql +=
                string.Format(
                    "VALUES ('{9}',999999,ISNULL((select TOP(1)[TRANS_KEY_LINE_NUM] from TRANS_HIST WHERE [GROUP_NUM]='999999' order by [TRANS_KEY_LINE_NUM] desc), 0) + 1,'{0}',-{1},'{7}','TMS','{8}','G','S',{2},'{3}','R','S',0.00,'{4}','N',0.00,0.00,0.00,0.0000000,1,'JICS: {2}','Create BU Tranactions','{0}','{5}','{6}')",
                    DateTime.Now, Balance, StudentID, SubsidiaryCode, ARCode, termcode[1], termcode[0], cmt, GetAccountCode(ARCode).AccountCode, SourceCode);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var retcode = command.ExecuteNonQuery();
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retcode;
        }

        protected int UpdatePaymentPlan(string StudentID, string Balance, string Term)
        {
            var termcode = Term.Split('_');
            var sql = string.Format(
                "UPDATE TRANS_HIST SET TRANS_AMT = -{0} where GROUP_NUM = '999999' AND ID_NUM = '{1}' AND CHG_TRM_TRAN_HIST = '{2}' AND CHG_YR_TRAN_HIST = '{3}'", Balance, StudentID, termcode[1], termcode[0]);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var retrow = command.ExecuteNonQuery();
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retrow;
        }

        public bool ValidateParameters(string StudentID, string LName, string PaymentAmount, string PaymentDate, string Source, string Comment, string BillingItem, string TmsPaymentId, string ARCode, string Term, ref xmlPaymentPlanPayment Error)
        {
            if (string.IsNullOrEmpty(StudentID))
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.NullStudentID,
                                                              (int)Errors.NullStudentID, StudentID);
                return false;
            }
            if (string.IsNullOrEmpty(BillingItem))
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.NullBillingItem,
                                                              (int)Errors.NullBillingItem);
                return false;
            }

            try
            {
                Convert.ToDouble(PaymentAmount);
            }
            catch (Exception)
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.PaymentAmountNotDouble,
                                                              (int)Errors.PaymentAmountNotDouble);
                return false;
            }

            try
            {
                Convert.ToDateTime(PaymentDate);
            }
            catch (Exception)
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.InvalidPaymentDate,
                                                              (int)Errors.InvalidPaymentDate);
                return false;
            }

            if (string.IsNullOrEmpty(ARCode))
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.ARCodeCannotBeNull,
                                                              (int)Errors.ARCodeCannotBeNull);
                return false;
            }

            var termcode = Term.Split('_');
            if (termcode.Length != 2)
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.TermCodeInvalid,
                                              (int)Errors.TermCodeInvalid);
                return false;
            }

            var studenthandler = new ValidateStudent();
            var studentinfo = studenthandler.GetValidStudentFromEx(StudentID);
            if (string.IsNullOrEmpty(studentinfo.LastName))
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentFailure(StudentID, Errors.InvalidStudentID,
                                                                          (int)Errors.InvalidStudentID, StudentID);
                return false;
            }

            if (string.IsNullOrEmpty(LName)) return true;

            if (studentinfo.LastName.Trim().ToLower() != LName.ToLower())
            {
                Error = TMSErrorHandler.SetPaymentPlanPaymentMismatchFailure(StudentID, Errors.StudentIDLNameMismatch,
                                                              (int)Errors.StudentIDLNameMismatch, studentinfo.LastName, LName);
                return false;
            }
            return true;

        }


    }
}