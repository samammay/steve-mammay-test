﻿using System;
using System.Data.SqlClient;
using TMS.DataObjects;
using TMS.Workers.Base;
using TMS.Helpers;

namespace TMS.Workers
{
    public enum ResponseEnum
    {
        UserNotFound,
        ERPError,
        OK
    }

    public static class StringExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            return tail_length >= source.Length ? source : source.Substring(source.Length - tail_length);
        }
    }



    public class Gateway : TMSWorkerBase
    {

        #region Private Properties
        //private readonly PaymentInfo PaymentInfo = new PaymentInfo();
        private xmlReturn xret = new xmlReturn();
        //private readonly xmlPaymentPlanPayment xppp = new xmlPaymentPlanPayment();
        private DateTime? _pd;
        private string _hostid { get; set; }
        //private string _lastname { get; set; }
        private double _paymentamount { get; set; }
        private string _arcode;
        private string _sourcecode;
        //private const string JOBNAME = "TMS Create BU Transactions";
        //private string _receiptnum;

        private DateTime? _paymentdate
        {
            get { return _pd ?? DateTime.Today; }

            set { _pd = value; }
        }

        //private string _source { get; set; }
        private string _comment { get; set; }
        private string _billingitem { get; set; }
        private string _tmspaymentid { get; set; }
        private string _term;
        private bool IsPaymentNegative;
        #endregion

        public xmlReturn PostPayment(string StudentID, string LName, double PaymentAmount, DateTime? PaymentDate,
                                     string Source, string Comment, string BillingItem, string TmsPaymentId,
                                     string ARCode, string Term, string SourceCode)
        {
            //populate properties

            //_receiptnum = !string.IsNullOrEmpty(ReceiptNumber) ? ReceiptNumber : "NULL";
            _hostid = xret.StudentId = StudentID;
            //_lastname = LName;
            _sourcecode = string.IsNullOrEmpty(SourceCode) ? "TM" : SourceCode;
            if (PaymentAmount < 0)
            {
                IsPaymentNegative = true;
            }
            _paymentamount = PaymentAmount;
            try
            {
                _paymentdate = PaymentDate;
            }
            catch (Exception)
            {
                _paymentdate = null;
            }
            //_source = Source;
            _comment = Comment;
            _billingitem = BillingItem;
            SetAccountCode();
            _tmspaymentid = TmsPaymentId;
            _arcode = ARCode;
            _term = Term;

            try
            {
                CreateNewPayment();
                
                TMSErrorHandler.SetPostPaymentSuccess(_hostid, ref xret);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return xret;
        }

        private void HandleException(Exception ex)
        {

            //SourceCode
            if (ex.Message.IndexOf("BU_REF_8219") != -1)
            {
                xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentInvalidSourceCode,
                                                      (int) Errors.GatewayPaymentInvalidSourceCode, _sourcecode);
                return;
            }

            //SubsidiaryCode BU_REF_11778
            if (ex.Message.IndexOf("BU_REF_11778") != -1)
            {
                xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentInvalidSubsidiaryCode,
                                                      (int)Errors.GatewayPaymentInvalidSubsidiaryCode, _billingitem);
                return;
            }

            //ARCode BU_REF_72639
            if (ex.Message.IndexOf("BU_REF_72639") != -1)
            {
                xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentInvalidARCode,
                                                      (int)Errors.GatewayPaymentInvalidARCode, _arcode);
                return;
            }
            xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentServerError, (int)Errors.GatewayPaymentServerError, ex.Message);
            
        }

        private void CreateNewPayment()
        {
            var cmt = !string.IsNullOrEmpty(_comment) ? _comment : "TMS Payment";
            var termcode = new string[2];
            if (!string.IsNullOrEmpty(_term))
                termcode = _term.Split('_');
            var tgid = new TransactionGroups().GetCurrentTransactionGroup(_comment, _sourcecode);
            var tgindex = new TransactionGroups().GetCurrentTGIndex(tgid, _paymentdate, _sourcecode);
            var accountcodes = GetAccountCode(_arcode);
            var payment = IsPaymentNegative ? _paymentamount.ToString().Replace("-", "") : "-" + _paymentamount.ToString().Replace("-", "");
            var offsetpayment = IsPaymentNegative ? "-" + _paymentamount.ToString().Replace("-", "") : _paymentamount.ToString().Replace("-", "");

            var sql = "INSERT INTO [TRANS_HIST]([SOURCE_CDE],[GROUP_NUM],[TRANS_KEY_LINE_NUM],[TRANS_DTE],[TRANS_AMT],[TRANS_DESC],[FOLIO],[ACCT_CDE],[ENCUMB_GL_FLAG],[ENCUMB_GL_TRANS_ST],[ID_NUM],[SUBSID_CDE],[OFFSET_FLAG],[SUBSID_TRANS_STS],[DISCOUNT],[AR_CDE],[OI_ALLOCATION],[UNDEFINED_11_2_FLD],[WORK_PERCENT],[FOR_CUR_DISCOUNT],[EXCHANGE_RATE],[PAYMENT_PLAN_NUM],[USER_NAME],[JOB_NAME],[JOB_TIME],[CHG_TRM_TRAN_HIST],[CHG_YR_TRAN_HIST], [ELIG_1098T])";
            sql +=
                string.Format(
                    "VALUES ('{9}',{10},{11},'{0}',{1},'{7}','TMS','{8}','G','S',{2},'{3}','R','S',0.00,'{4}','N',0.00,0.00,0.00,0.0000000,1,'JICS: {2}','TMS Create BU Transactions','{0}','{5}','{6}', 'N')",
                    DateTime.Now, payment, _hostid, SetAccountCode(), _arcode, termcode[1], termcode[0], cmt, accountcodes.AccountCode, _sourcecode, tgid, tgindex);
            sql += "INSERT INTO [TRANS_HIST]([SOURCE_CDE],[GROUP_NUM],[TRANS_KEY_LINE_NUM],[TRANS_DTE],[TRANS_AMT],[TRANS_DESC],[FOLIO],[ACCT_CDE],[ENCUMB_GL_FLAG],[ENCUMB_GL_TRANS_ST],[ID_NUM],[OFFSET_FLAG],[SUBSID_TRANS_STS],[DISCOUNT],[AR_CDE],[OI_ALLOCATION],[UNDEFINED_11_2_FLD],[WORK_PERCENT],[FOR_CUR_DISCOUNT],[EXCHANGE_RATE],[PAYMENT_PLAN_NUM],[USER_NAME],[JOB_NAME],[JOB_TIME],[CHG_TRM_TRAN_HIST],[CHG_YR_TRAN_HIST], [ELIG_1098T])";
            sql +=
                string.Format(
                    "VALUES ('{8}',{9},{10},'{0}',{1},'{6}','TMS','{7}','G','S',{2},'O','S',0.00,'{3}','N',0.00,0.00,0.00,0.0000000,1,'JICS: {2}','TMS Create BU Transactions','{0}','{4}','{5}', 'N')",
                    DateTime.Now, offsetpayment, _hostid, _arcode, termcode[1], termcode[0], cmt, accountcodes.OffsetAccountCode, _sourcecode, tgid, tgindex + 1);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentServerError, (int)Errors.GatewayPaymentServerError, ex.InnerException.Message);
            }
            try
            {
                var command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
                xret.TransactionGroup = _sourcecode + "_" + tgid;
                xret.Cashier = "TMSUser";  //change if we ever allow different users to post
                xret.TransGroupCreationDate = new TransactionGroups().GetTransactionGroupDate(_sourcecode, tgid);
//                -Group number (so TM_1, etc.)
//-Cashier (think this will always be TMSUser)
//-Group creation time

            }
            catch (Exception ex)
            {
                throw (ex);
                //xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentServerError, (int)Errors.GatewayPaymentServerError, ex.InnerException.Message);
            }
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                xret = TMSErrorHandler.SetPostPaymentFailure(_hostid, Errors.GatewayPaymentServerError, (int)Errors.GatewayPaymentServerError, ex.InnerException.Message);
            }
        }

        #region Worker Methods
        //defines gateway to be used
        private string SetAccountCode()
        {
            return string.IsNullOrEmpty(_billingitem) ? "AR" : _billingitem;
        }

        public bool ValidateParameters(string StudentID, string LName, string PaymentAmount, string PaymentDate, string Source, string Comment, string BillingItem, string TmsPaymentId, string ARCode, string Term, ref xmlReturn Error)
        {
            if (string.IsNullOrEmpty(StudentID))
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.NullStudentID,
                                                              (int)Errors.NullStudentID, StudentID);
                return false;
            }
            if (string.IsNullOrEmpty(BillingItem))
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.NullBillingItem,
                                                              (int)Errors.NullBillingItem);
                return false;
            }
            if (string.IsNullOrEmpty(TmsPaymentId))
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.NullPaymentId,
                                                              (int)Errors.NullPaymentId);
                return false;
            }
            try
            {
                Convert.ToDouble(PaymentAmount);
            }
            catch (Exception)
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.PaymentAmountNotDouble,
                                                              (int)Errors.PaymentAmountNotDouble);
                return false;
            }

            try
            {
                Convert.ToDateTime(PaymentDate);
            }
            catch (Exception)
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.InvalidPaymentDate,
                                                              (int)Errors.InvalidPaymentDate);
                return false;
            }

            if (string.IsNullOrEmpty(ARCode))
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.ARCodeCannotBeNull,
                                                              (int)Errors.ARCodeCannotBeNull);
                return false;
            }

            var studenthandler = new ValidateStudent();
            var studentinfo = studenthandler.GetValidStudentFromEx(StudentID);
            if (string.IsNullOrEmpty(studentinfo.LastName))
            {
                Error = TMSErrorHandler.SetPostPaymentFailure(StudentID, Errors.InvalidStudentID,
                                                                          (int)Errors.InvalidStudentID, StudentID);
                return false;
            }

            if (string.IsNullOrEmpty(LName)) return true;

            if (studentinfo.LastName.Trim().ToLower() != LName.ToLower())
            {
                Error = TMSErrorHandler.SetPostPaymentMismatchFailure(StudentID, Errors.StudentIDLNameMismatch,
                                                              (int)Errors.StudentIDLNameMismatch, studentinfo.LastName, LName);
                return false;
            }
            return true;
        }

        public bool ValidateParameters(string startDate, string endDate, string eligibleItemId, ref StudentDataList Error)
        {

            try
            {
                Convert.ToDateTime(startDate);
            }
            catch (Exception)
            {
                Error = TMSErrorHandler.SetAccountBalanceChangedFailure(Errors.AccountBalanceChangedInvalidStartDate,
                                              (int)Errors.AccountBalanceChangedInvalidStartDate);
                return false;
            }
            try
            {
                Convert.ToDateTime(endDate);
            }
            catch (Exception)
            {
                Error = TMSErrorHandler.SetAccountBalanceChangedFailure(Errors.AccountBalanceChangedInvalidEndDate,
                                              (int)Errors.AccountBalanceChangedInvalidEndDate);
                return false;
            }
            return true;
        }

        #endregion

    }

}