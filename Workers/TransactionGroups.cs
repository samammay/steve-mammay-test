﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class TransactionGroups : TMSWorkerBase
    {

        public string GetCurrentTransactionGroup(string name, string sourcecode)
        {
            return ProcessTransactionGroupRequest(name, sourcecode) != string.Empty ? ProcessTransactionGroupRequest(name, sourcecode) : CreateNewTransactionGroup(name, sourcecode);
        }

        public int GetCurrentTGIndex(string groupnum, DateTime? date, string sourcecode)
        {
            if (string.IsNullOrEmpty(sourcecode)) sourcecode = "TM";
            //select from where date == today and name == name
            var sql = string.Format("select * from TRANS_HIST WHERE GROUP_NUM = '{0}' AND SOURCE_CDE = '{2}' AND DateDiff(\"day\", TRANS_DTE, '{1}') = 0 ORDER BY TRANS_KEY_LINE_NUM DESC", groupnum, date, sourcecode);
            


            DebugInfo = sql;
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();
            var ids = new List<string>();

            while (reader.Read())
            {
                ids.Add(((int)reader["TRANS_KEY_LINE_NUM"]).ToString());
            }

            //load the datatable with the SqlDataReader
            //DataTable dataTable = new DataTable();
            //dataTable.Load(reader);

            //convert the datatable to xml
            //StringWriter xmlWriter = new StringWriter();
            //dataTable.WriteXml(xmlWriter);

            //return the xml
            //EventLog.WriteEntry(appName, sql + "<br />" + xmlWriter, EventLogEntryType.Warning);


            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Barry College testing

            var intlist = ids.Select(item => Convert.ToInt32(item)).OrderByDescending(x => x).ToList();
            
            var retvalue = 1;
            if (intlist.Count > 0)
            {
                retvalue = intlist[0] + 1;


            }
            return retvalue;
        }

        public DateTime GetTransactionGroupDate(string sourcecode, string TGID)
        {
            var sql = string.Format(
                "select JOB_TIME from TRANS_BATCH_CTL WHERE SOURCE_CDE = '{0}' AND GROUP_NUM = {1}", sourcecode, TGID);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();
            var ids = new List<DateTime>();

            while (reader.Read())
            {
                var item = Convert.ToDateTime(reader["JOB_TIME"].ToString());
                ids.Add(item);
                //ids.Add(((DateTime)reader["GROUP_NUM"]).ToString());
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (ids.Count != 1) throw new Exception(string.Format("The GetTransactionGroupDate found more/less than one transaction group with code of {0} and ID {1}", sourcecode, TGID));
            return ids[0];
        }

        private string ProcessTransactionGroupRequest(string name, string sourcecode)
        {

            //select from where date == today and name == name
            var sql = string.Format("select * from TRANS_BATCH_CTL WHERE GRP_DESC = '{0}' AND SOURCE_CDE = '{1}' AND DateDiff(\"day\", GRP_DTE, getDate()) = 0 ", name, sourcecode);

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();
            var ids = new List<string>();

            while (reader.Read())
            {
                ids.Add(((int)reader["GROUP_NUM"]).ToString());
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ids.Count == 0 ? string.Empty : ids[0];
        }

        public string CreateNewTransactionGroup(string name, string sourcecode)
        {
            //var sql = string.Format("select TOP(1) GROUP_NUM from TRANS_BATCH_CTL ORDER BY GROUP_NUM");
            var sql = "INSERT INTO [TRANS_BATCH_CTL]([SOURCE_CDE],[GROUP_NUM],[GRP_DESC],[GRP_DTE],[GRP_STS],[SUMMARIZED],[USER_NAME],[JOB_NAME],[JOB_TIME] )";
            sql +=
                string.Format(
                    "VALUES ('{2}',ISNULL((select [GRP_NUM_CTL] from source_master WHERE SOURCE_CDE = '{2}'), 0),'{0}','{1}','S','N','TE_ADMIN', 'TMS Payment', getDate())",
                    name, DateTime.Today, sourcecode);

            var updatesql =
                string.Format(
                    "UPDATE [TRANS_BATCH_CTL] SET GRP_STS = 'U' WHERE SOURCE_CDE = '{1}' AND GRP_DESC = '{0}' AND [GRP_STS] = 'S'",
                    name, sourcecode);

            var incrementsql = string.Format("UPDATE source_master SET [GRP_NUM_CTL] = ISNULL((select [GRP_NUM_CTL] from source_master WHERE SOURCE_CDE = '{0}'), 0) + 1, JOB_NAME = 'TMS Payment', JOB_TIME = getDate() WHERE SOURCE_CDE = '{0}'", sourcecode);

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }
            var updatecommand = new SqlCommand(updatesql, conn);
            updatecommand.ExecuteNonQuery();

            var command = new SqlCommand(sql, conn);
            command.ExecuteNonQuery();

            var incrementcommand = new SqlCommand(incrementsql, conn);
            incrementcommand.ExecuteNonQuery();
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //close yesterday
            return ProcessTransactionGroupRequest(name, sourcecode);

        }

        /*     SELECT TOP 1000 [SOURCE_CDE]
           ,[GROUP_NUM]
           ,[GRP_DESC]
           ,[GRP_DTE]
           ,[GRP_STS]
           ,[CHG_CDE]
           ,[EXT_FIN_TRANSMISSION_STS]
           ,[EXT_FIN_TRANSMISSION_DTE]
           ,[SUMMARIZED]
           ,[USER_NAME]
           ,[JOB_NAME]
           ,[JOB_TIME]
       FROM [TmsEDmo].[dbo].[TRANS_BATCH_CTL]*/

    }
}