﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class ActiveStudentHandler :TMSWorkerBase
    {
        private DateTime termStartDate;
        private DateTime termEndDate;
        private ActiveStudents activeStudents;

        public ActiveStudents GetActiveStudents(string Term)
        {
            if (!new TermCodeConverter().PopulateDates(Term, ref termStartDate, ref termEndDate))
                return TMSErrorHandler.SetActiveStudentsFailure(Errors.InvalidTermForActiveStudents,
                                                                (int) Errors.InvalidTermForActiveStudents, Term);
            activeStudents= new ActiveStudents();
            GetActiveStudentsFromDatabase();
            return activeStudents;
        } 




        private void GetActiveStudentsFromDatabase()
        {
            var sql = string.Format("select DISTINCT ID_NUM from TRANS_HIST WHERE TRANS_DTE < '{0}' AND TRANS_DTE > '{1}'", termEndDate.ToString(), termStartDate.ToString());

            activeStudents.ActiveStudentIds = new List<string>();
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                activeStudents.ActiveStudentIds.Add(reader["ID_NUM"].ToString());
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (activeStudents.ActiveStudentIds.Count == 0) TMSErrorHandler.SetActiveStudentSuccessWithNoActiveStudents(ref activeStudents);
            else
            {
                TMSErrorHandler.SetActiveStudentSuccess(ref activeStudents);
            }

        }
    }


}