﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class AccountBalance : TMSWorkerBase
    {
        private AccountDetailList accountDetails;
        private string _hostid;
        public AccountDetailList GetAccountBalance(string hostid, List<string> includeItemId, List<string> excludeItemId, List<string> eligibleCodeList, List<string> ineligibleCodeList, string Term)
        {
            if (string.IsNullOrEmpty(hostid)) return null;
            _hostid = hostid;
            accountDetails = new AccountDetailList { AccountDetails = new List<AccountDetails>(), StudentId = hostid };
            if (GetAccountDetails(hostid, ref accountDetails))
            {

                if (eligibleCodeList.Count > 0)
                {
                    foreach (var item in accountDetails.AccountDetails)
                    {
                        foreach (var trans in item.Transactions)
                        {
                            bool ok = false;
                            if (!UseSubsid())
                            {
                                foreach (var test in eligibleCodeList.Where(test => trans.AR_Code == test || trans.ChargeFeeCode == test))
                                {
                                    ok = true;
                                }
                            }
                            else
                            {
                                foreach (var test in eligibleCodeList.Where(test => trans.SubsidiaryCode == test))
                                {
                                    ok = true;
                                }
                            }
                            trans.PaymentPlanEligible = ok.ToString();
                        }
                    }
                }

                if (ineligibleCodeList.Count > 0)
                {
                    foreach (var item in accountDetails.AccountDetails)
                    {
                        foreach (var trans in item.Transactions)
                        {
                            bool ok = true;
                            if (!UseSubsid())
                            {
                                foreach (var test in ineligibleCodeList.Where(test => trans.AR_Code == test || trans.ChargeFeeCode == test))
                                {
                                    ok = false;
                                }
                            }
                            else
                            {
                                foreach (var test in ineligibleCodeList.Where(test => trans.SubsidiaryCode == test))
                                {
                                    ok = false;
                                }
                            }
                            trans.PaymentPlanEligible = ok.ToString();
                        }
                    }
                }
                if (eligibleCodeList.Count == 0 && ineligibleCodeList.Count == 0)
                {
                    foreach (var trans in accountDetails.AccountDetails.SelectMany(item => item.Transactions))
                    {
                        trans.PaymentPlanEligible = "True";
                    }
                }




                //var adOut = new AccountDetailList { AccountDetails = new List<AccountDetails>() };
                var tl = new List<Transaction>();
                if (includeItemId.Count > 0)
                {
                    if (!UseSubsidForInclude())
                        tl.AddRange(from y in accountDetails.AccountDetails from trans in y.Transactions from test in includeItemId.Where(test => trans.AR_Code == test || trans.ChargeFeeCode == test) select trans);
                    else
                    {
                        tl.AddRange(from y in accountDetails.AccountDetails from trans in y.Transactions from test in includeItemId.Where(test => trans.SubsidiaryCode == test) select trans);

                    }
                }
                else if (excludeItemId.Count > 0)
                {
                    //adOut = new AccountDetailList {AccountDetails = new List<AccountDetails>()};
                    foreach (var trans in accountDetails.AccountDetails.SelectMany(y => y.Transactions))
                    {
                        var ok = true;
                        if (!UseSubsidForInclude())
                        {
                            foreach (var test in excludeItemId.Where(test => trans.AR_Code == test))
                            {
                                ok = false;
                            }
                            foreach (var test in excludeItemId.Where(test => trans.ChargeFeeCode == test))
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            foreach (var test in excludeItemId.Where(test => trans.SubsidiaryCode == test))
                            {
                                ok = false;
                            }
                        }

                        if (ok) tl.Add(trans);
                        //tl.AddRange(excludeItemId.Where(test => trans.AR_Code != test && trans.AR_Code != "").Select(test => trans));
                        //tl.AddRange(excludeItemId.Where(test => trans.ChargeFeeCode != test && trans.ChargeFeeCode != "").Select(test => trans));
                    }

                }
                else
                {
                    foreach (var q in accountDetails.AccountDetails)
                    {
                        tl.AddRange(q.Transactions);
                    }
                }

                accountDetails.AccountDetails[0].Transactions = tl.ToArray();
                if (includeItemId.Count > 0 || excludeItemId.Count > 0) RecomputeAccountBalance(ref accountDetails);

                foreach (var trans in accountDetails.AccountDetails.SelectMany(ad => ad.Transactions).Where(trans => string.IsNullOrEmpty(trans.TransactionTermID)))
                {
                    trans.TransactionTermID = GetTransTermId(trans.TransactionDate);
                    if (trans.TransactionTermID == "_") trans.TransactionTermID = "TEST";
                }

                if (!string.IsNullOrEmpty(Term)) accountDetails.AccountDetails[0].Transactions = FilterTerms(tl, Term).ToArray();
                if (accountDetails.MessageNbr > 0) return accountDetails;
                foreach (var a in accountDetails.AccountDetails)
                {
                    foreach (var t in a.Transactions)
                    {
                        if (t.TransactionType == "Payment" && t.Amount < 0) t.Amount = t.Amount * -1;
                    }

                }
                TMSErrorHandler.SetTransactionsSuccess(hostid, ref accountDetails);
                return accountDetails;
            }
            return null;
        }

        private List<Transaction> FilterTerms(List<Transaction> tl, string Term)
        {
            var returnList = new List<Transaction>();
            //var termStartDate = new DateTime();
            //var termEndDate = new DateTime();
            //if (!new TermCodeConverter().PopulateDates(Term, ref termStartDate, ref termEndDate))
            //    accountDetails = TMSErrorHandler.SetTransactionsFailure(_hostid, Errors.InvalidTermForActiveStudents,
            //                                                    (int)Errors.InvalidTermForActiveStudents, Term);
            foreach (var item in tl)
            {
                if (item.TransactionTermID == Term)
                    returnList.Add(item);
            }
            return returnList;
        }

        private static void RecomputeAccountBalance(ref AccountDetailList accountDetails)
        {
            accountDetails.AccountDetails[0].AccountBalance = 0;
            foreach (var ta in accountDetails.AccountDetails[0].Transactions)
            {
                accountDetails.AccountDetails[0].AccountBalance += ta.Amount;
            }
        }

        private TermData[] termData { get; set; }

        private string GetTransTermId(DateTime dte)
        {
            var ret = "No Term Given";
            if (termData == null || termData.Length == 0)
                termData = new AvailableTerms().GetTerms();

            foreach (var term in termData.Where(term => dte > term.termStartDate && dte < term.termEndDate))
            {
                ret = term.termCode;
            }
            return ret == "_" ? string.Empty : ret;
        }

        public AccountDetailList GetAccountBalanceOnly(string hostid, List<string> includeItemId, List<string> excludeItemId, List<string> eligibleItemId)
        {
            if (string.IsNullOrEmpty(hostid)) return null;
            var accountDetails = new AccountDetailList { AccountDetails = new List<AccountDetails>(), StudentId = hostid };
            if (GetAccountDetails(hostid, ref accountDetails))
            {

                foreach (var item in accountDetails.AccountDetails)
                {
                    item.Transactions = null;
                }
                var adOut = new AccountDetailList { AccountDetails = new List<AccountDetails>() };
                if (includeItemId.Count > 0)
                {
                    foreach (var y in accountDetails.AccountDetails)
                    {
                        adOut.AccountDetails.AddRange(from uu in includeItemId where y.ID == uu select y);
                    }
                }
                else if (excludeItemId.Count > 0)
                {
                    adOut = new AccountDetailList { AccountDetails = new List<AccountDetails>() };
                    foreach (var y in accountDetails.AccountDetails)
                    {
                        var add = true;
                        foreach (var uu in excludeItemId.Where(uu => uu == y.ID))
                        {
                            add = false;
                        }
                        if (add) adOut.AccountDetails.Add(y);

                    }

                }
                else
                {
                    adOut = accountDetails;
                }
                return adOut;
            }
            return null;
        }

        public string[] GetStudentsWithBalanceChange(DateTime startDate, DateTime endDate, string[] eligibleItemId, string[] ineligibleItemId)
        {
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source, FaultCode.Server);
            }
            var ids = eligibleItemId;
            if (ids.Count() == 1 && ids[0] == string.Empty)
                ids = null;

            var inelids = ineligibleItemId;
            if (inelids.Count() == 1 && inelids[0] == string.Empty)
                inelids = null;

            var command = new SqlCommand(GetStudentsWithBalanceChangeSQL(startDate, endDate, ids, inelids), conn);
            SqlDataReader reader = command.ExecuteReader();
            var hostids = new List<string>();

            while (reader.Read())
            {
                hostids.Add(((int)reader["HostID"]).ToString());
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source, FaultCode.Server);
            }

            return hostids.Distinct().ToArray();
        }

        private static string GetStudentsWithBalanceChangeSQL(DateTime startDate, DateTime endDate, ICollection<string> codes, ICollection<string> ineligiblecodes)
        {
            var code = string.Empty;
            var notin = string.Empty;
            if (codes != null && codes.Count > 0)
            {
                code = codes.Aggregate(code, (current, x) => current + ("'" + x.Trim() + "',"));
                code = code.Substring(0, code.Length - 1);
            }

            if (ineligiblecodes != null && ineligiblecodes.Count > 0)
            {
                code = ineligiblecodes.Aggregate(code, (current, x) => current + ("'" + x.Trim() + "',"));
                code = code.Substring(0, code.Length - 1);
                notin = "not ";
            }
            //else code = "'AR'";
            var sb = new StringBuilder();
            sb.Append("	SELECT th.id_num as HostID, th.trans_desc AS TransWorkDesc, th.trans_dte AS WorkTransDte, th.trans_amt AS WorkTransAmt, th.subsid_trans_sts AS WorkTransSts, th.third_pty_bill_id AS WorkThirdPtyBillId, th.source_cde AS SourceCde 	");
            sb.Append("	FROM trans_hist AS th 	");
            if (code != string.Empty)
                sb.AppendFormat("	WHERE ( ( ( th.subsid_cde {1}in ({0}) AND th.offset_flag = 'R')", code, notin);
            else
            {
                sb.Append("	WHERE ( ( ( th.id_num > 0 AND th.offset_flag = 'R')");
            }
            sb.Append("	AND ( th.subsid_trans_sts IN ('C', 'H', 'Y', 'U', 'S')))	");
            sb.AppendFormat("	AND (th.trans_dte >= '{0}' AND th.trans_dte <= '{1}'))", startDate, endDate);
            return sb.ToString();
        }


        public bool ValidateParameters(string studentid, ref AccountDetailList Error)
        {
            if (string.IsNullOrEmpty(studentid))
            {
                Error = TMSErrorHandler.SetAccountBalanceFailure(studentid, Errors.NullStudentID,
                                              (int)Errors.NullStudentID, studentid);
                return false;
            }
            var studenthandler = new ValidateStudent();
            var studentinfo = studenthandler.GetValidStudentFromEx(studentid);
            if (string.IsNullOrEmpty(studentinfo.LastName))
            {
                Error = TMSErrorHandler.SetAccountBalanceFailure(studentid, Errors.InvalidStudentID,
                                                                          (int)Errors.InvalidStudentID, studentid);
                return false;
            }
            return true;
        }


    }
}