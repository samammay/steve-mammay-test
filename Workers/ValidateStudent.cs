﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Jenzabar.CRM.Deserializers;
using Jenzabar.CRM.Utility;
using Jenzabar.Portal.Framework;
using Jenzabar.Portal.Framework.Facade.Ldap;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;
using StructureMap;

namespace TMS.Workers
{
    public class ValidateStudent : TMSWorkerBase
    {

        private string _hostid;
        private string _primaryaddresscode;

        public TMSStudentInfo GetValidStudent(string hostid)
        {
            _hostid = hostid;
            return GetPersionalInfoNew();
            //if (!_isAuth)
            //{
            //    return "failed authentication";
            //}
            var studentinfo = new TMSStudentInfo();
            if (!FindUser(hostid, ref studentinfo))
            {
                throw new SoapException("User Not Found", SoapException.ServerFaultCode);
            }
            studentinfo.StudentId = hostid;
            return studentinfo;
            //var xmlserializer = new XmlSerializer(studentinfo.GetType());
            //using (var writer = new StringWriter())
            //{
            //    xmlserializer.Serialize(writer, studentinfo);
            //    return writer.ToString();
            //}
        }

        public TMSStudentInfo GetValidStudentFromEx(string hostid)
        {
            var studentinfo = new TMSStudentInfo {StudentId = hostid};
            var sql = string.Format("SELECT TOP 1 LAST_NAME, FIRST_NAME FROM NAME_MASTER WHERE ID_NUM = '{0}'", hostid);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                studentinfo.LastName = reader["LAST_NAME"].ToString().Trim();
                studentinfo.FirstName = reader["FIRST_NAME"].ToString().Trim();
            }
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return studentinfo;
        }

        //public TMSStudentInfo GetValidStudentInfo(string hostid)
        //{
        //    var studentinfo = new TMSStudentInfo();
        //    return !FindUser(hostid, ref studentinfo) ? null : studentinfo;
        //}

        public List<PortalUser> GetAllStudents()
        {
            var facade = ObjectFactory.GetInstance<PortalUserFacade>();
            return facade.FindAll().ToList();
        }

        private bool FindUser(string hostid, ref TMSStudentInfo si)
        {
            var facade = ObjectFactory.GetInstance<PortalUserFacade>();
            var pu = facade.FindByHostID(hostid) ?? facade.FindByHostID(TryPad(hostid));
            if (pu == null) return false;
            MapToObject(pu, ref si);
            return true;
        }

        private string TryPad(string input)
        {
            var output = input;
            var cnt = input.Length;
            for(var i = cnt; i < 11; i++)
            {
                output = "0" + output;
            }
            return output;
        }

        private void MapToObject(PortalUser pu, ref TMSStudentInfo si)
        {
            si.FirstName = pu.FirstName;
            si.MiddleName = pu.MiddleName;
            si.LastName = pu.LastName;
            si.NameForDisplay = pu.FirstName + " " + pu.MiddleName + " " + pu.LastName;

            //erp info
            var pi = GetPersonalInformation(pu);
            if (pi.Gender != null) si.Gender = pi.Gender.Value;
            if (pi.BirthDate != null) si.BirthDate = pi.BirthDate.Value;

            //si. = pi.EmailAddress;
            //si. = pi.Ethnicity;
            //si = pi.MaritalStatus;
            //var x = pi.SpouseName;

            //current address
            
            si.Contacts = new Contact[2];
            //contact info
            var phone = new Contact();
            if (pi.Phone != null)
            {
                phone.ContactType = "Phone";
                phone.NumEmail = pi.Phone.Value;
                si.Contacts[0] = phone;
            }

            var email = new Contact();
            if (pi.EmailAddress != null)
            {
                email.ContactType = "Email";
                email.NumEmail = pi.EmailAddress.Value;
                si.Contacts[1] = email;
            }
            if (pi.CurrentAddress == null) return;

            var ca = new Address();
            if (pi.CurrentAddress.AddressLine1 != null) ca.AddressLine1 = pi.CurrentAddress.AddressLine1.Value;
            if (pi.CurrentAddress.AddressLine2 != null) ca.AddressLine2 = pi.CurrentAddress.AddressLine2.Value;
            if (pi.CurrentAddress.AddressLine3 != null) ca.AddressLine3 = pi.CurrentAddress.AddressLine3.Value;
            if (pi.CurrentAddress.City != null) ca.City = pi.CurrentAddress.City.Value;
            if (pi.CurrentAddress.Country != null) ca.Country = pi.CurrentAddress.Country.Value;
            ca.IsPrimaryAddress = "true";
            ca.Type = "Primary Address";
            if (pi.CurrentAddress.Zip != null) ca.PostCode = pi.CurrentAddress.Zip.Value;
            if (pi.CurrentAddress.State != null) ca.State = pi.CurrentAddress.State.Value;

            int z = pi.OtherAddresses.Length > 0 ? pi.OtherAddresses.Length + 1 : 1;
            var add = new Address[z];
            if (si.Addresses == null) si.Addresses = add;
                si.Addresses[0] = ca;

                for (var q = 0; q < pi.OtherAddresses.Length; q++ )
                {
                    var oth = new Address();
                    if (pi.OtherAddresses[q].AddressTypeDescription != null) oth.Type = pi.OtherAddresses[q].AddressTypeDescription.Value;
                    if (pi.OtherAddresses[q].AddressLine1 != null) oth.AddressLine1 = pi.OtherAddresses[q].AddressLine1.Value;
                    if (pi.OtherAddresses[q].AddressLine2 != null) oth.AddressLine2 = pi.OtherAddresses[q].AddressLine2.Value;
                    if (pi.OtherAddresses[q].AddressLine3 != null) oth.AddressLine3 = pi.OtherAddresses[q].AddressLine3.Value;
                    if (pi.OtherAddresses[q].City != null) oth.City = pi.OtherAddresses[q].City.Value;
                    if (pi.OtherAddresses[q].Country != null) oth.Country = pi.OtherAddresses[q].Country;
                    oth.IsPrimaryAddress = "false";
                    if (pi.OtherAddresses[q].Zip != null) oth.PostCode = pi.OtherAddresses[q].Zip.Value;
                    if (pi.OtherAddresses[q].State != null) oth.State = pi.OtherAddresses[q].State.Value;
                    si.Addresses[q+1] = oth;
                }

            //var x = pi.OtherAddresses;
            //var x = pi.CurrentEmployment;
            //var x = pi.EducationInformation;
            //var x = pi.OtherNames;
            
            
        }

        private PersonalInfo GetPersonalInformation(PortalUser pu)
        {
            var PluginParam = new object[1];
            PluginParam[0] = pu.HostID;
            string strXML = "";
            string strError = "";

            var myinfo = new Jenzabar.ERP.MyInfo();
            myinfo.GetPersonalInformation(pu.HostID, ref strXML, ref strError);
            PersonalInfo pi = null;
            if (strXML.Trim() != "")
            {
                strXML = strXML.Replace("ERP>", "PersonalInfo>");
                strXML = strXML.Replace("OtherAddress>", "OtherAddresses>");
                strXML = strXML.Replace("<Email ", "<EmailAddress ");
                strXML = strXML.Replace("/Email>", "/EmailAddress>");
                pi = (PersonalInfo)PlugIn.MapXMLToObject(strXML, new XmlSerializer(typeof(PersonalInfo)));
            }
            return pi;
        }

        private TMSStudentInfo GetPersionalInfoNew()
        {

            var pi = new TMSStudentInfo {StudentId = _hostid};
            try
            {
                GetBasicInfo(ref pi);
                if (string.IsNullOrEmpty(pi.LastName))
                {
                    pi = TMSErrorHandler.SetGetStudentInfoFailure(_hostid, Errors.InvalidStudentID, (int)Errors.InvalidStudentID, _hostid);
                    return pi;  
                }
                GetPrimaryAddress(ref pi);
                GetBirthDate(ref pi);
                GetOtherAddresses(ref pi);
                GetFinalInfoCleaned(ref pi);
            }
            catch (Exception ex)
            {
                pi = TMSErrorHandler.SetGetStudentInfoFailure(_hostid, Errors.GetStudentInfoError, (int)Errors.GetStudentInfoError, ex.Message);
                return pi;
            }
            TMSErrorHandler.SetGetStudentInfoSuccess(ref pi);
            return pi;
        }

        private void GetBasicInfo(ref TMSStudentInfo pi)
        {
            var sql =
                string.Format(
                    "SELECT nm.name_format, nm.first_name, nm.middle_name, nm.last_name, nm.nickname, nm.birth_name, nm.preferred_name, nm.email_address, nm.prefix, td_prefix_view.description d_prefix_desc, nm.suffix, nm.current_address FROM name_master nm LEFT OUTER JOIN td_prefix_view td_prefix_view ON nm.prefix = td_prefix_view.value WHERE nm.id_num = {0}",
                    _hostid);
            

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                //ret.AccountCode = reader["ACCT_CDE"].ToString();
                //ret.OffsetAccountCode = reader["OFFSET_ACCT_CDE"].ToString();
                pi.FirstName = reader["first_name"].ToString();
                pi.MiddleName = reader["middle_name"].ToString();
                pi.LastName = reader["last_name"].ToString();
                pi.NameForDisplay = reader["first_name"] + " " + reader["middle_name"] + " " + reader["last_name"];
                _primaryaddresscode = reader["current_address"].ToString();
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GetPrimaryAddress(ref TMSStudentInfo pi)
        {

            var sql =
                string.Format(
                    "SELECT ad.addr_line_1, ad.addr_line_2, ad.addr_line_3, ad.city, ad.state, sv.description state_desc, ad.zip, ad.country, cv.description country_desc, ad.phone, ad.extension, ad.county, ad.locality FROM address_master ad LEFT OUTER JOIN td_country_view cv ON ad.country = cv.value LEFT OUTER JOIN td_state_view sv ON ad.state = sv.value WHERE ad.id_num = {0} AND ad.addr_cde = '{1}'", _hostid, _primaryaddresscode);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();
            var lst = new List<Address>();
            var contacts = new List<Contact>();
            var phone = new Contact();
            while (reader.Read())
            {
                var ca = new Address();
                ca.AddressLine1 = reader["addr_line_1"].ToString();
                ca.AddressLine2 = reader["addr_line_2"].ToString();
                ca.AddressLine3 = reader["addr_line_3"].ToString();
                ca.City = reader["city"].ToString();
                ca.Country = reader["country"].ToString();
                ca.IsPrimaryAddress = "true";
                ca.Type = "Primary Address";
                ca.PostCode = reader["zip"].ToString();
                ca.State = reader["state"].ToString();
                lst.Add(ca);
                                phone.ContactType = "Phone";
                                phone.NumEmail = reader["phone"].ToString();
                contacts.Add(phone);
            }
            pi.Addresses = lst.ToArray();
            pi.Contacts = contacts.ToArray();

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GetBirthDate(ref TMSStudentInfo pi)
        {
            var sql =
                string.Format(
                    "SELECT bm.birth_dte, bm.marital_sts, spouse_name.pre_first_middle_last SpouseName, ethnic_grouping.table_desc as Ethnicity, gender_grouping.table_desc as Gender FROM biograph_master bm LEFT OUTER JOIN name_format_web_view spouse_name ON bm.spouse_id = spouse_name.id_num LEFT OUTER JOIN TABLE_DETAIL ethnic_grouping ON (bm.Ethnic_Group = ethnic_grouping.table_value AND ethnic_grouping.column_name = 'ethnic_group') LEFT OUTER JOIN table_detail gender_grouping ON (bm.Gender = gender_grouping.table_value AND gender_grouping.column_name = 'gender') WHERE bm.id_num = {0}",_hostid);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                //ret.AccountCode = reader["ACCT_CDE"].ToString();
                //ret.OffsetAccountCode = reader["OFFSET_ACCT_CDE"].ToString();
                var bd = Convert.ToDateTime(reader["birth_dte"].ToString());
                pi.BirthDate = bd.ToShortDateString();
                pi.Gender = reader["Gender"].ToString();
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetOtherAddresses(ref TMSStudentInfo pi)
        {
            var sql =
                string.Format(
                    "SELECT address_code.table_desc AddrDesc, am.addr_line_1, am.addr_line_2, am.addr_line_3, am.city, am.state, am.zip, country_desc.table_desc CountryDesc, am.phone, am.extension, sv.description state_desc FROM address_master am LEFT OUTER JOIN table_detail address_code ON am.addr_cde = address_code.table_value AND address_code.column_name = 'addr_cde' LEFT OUTER JOIN table_detail country_desc ON am.country = country_desc.table_value AND country_desc.column_name = 'country' LEFT OUTER JOIN td_state_view sv ON am.state = sv.value WHERE am.id_num = {0}AND am.addr_cde <> '{1}'", _hostid, _primaryaddresscode);
            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();
            var lst = new List<Address>();
            if (pi.Addresses != null && pi.Addresses.Length > 0)
            {
                lst.AddRange(pi.Addresses);
            }
            while (reader.Read())
            {
                var ca = new Address();
                ca.AddressLine1 = reader["addr_line_1"].ToString();
                ca.AddressLine2 = reader["addr_line_2"].ToString();
                ca.AddressLine3 = reader["addr_line_3"].ToString();
                ca.City = reader["city"].ToString();
                ca.Country = reader["CountryDesc"].ToString();
                ca.IsPrimaryAddress = "false";
                ca.Type = reader["AddrDesc"].ToString();
                ca.PostCode = reader["zip"].ToString();
                ca.State = reader["state"].ToString();
                lst.Add(ca);
                if (ca.Type.ToLower().IndexOf("email", StringComparison.Ordinal) < 0) continue;
                var contacts = pi.Contacts.ToList();
                var email = new Contact();
                email.ContactType = "Email";
                email.NumEmail = reader["addr_line_1"].ToString();
                contacts.Add(email);
                pi.Contacts = contacts.ToArray();
            }
            pi.Addresses = lst.ToArray();

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetFinalInfoCleaned(ref TMSStudentInfo pi)
        {

        }


        public bool ValidateParameters(string studentid, ref TMSStudentInfo Error)
        {
            if (string.IsNullOrEmpty(studentid))
            {
                Error = TMSErrorHandler.SetValidateStudentFailure(studentid, Errors.NullStudentID,
                                                                  (int) Errors.NullStudentID, studentid);
                return false;
            }
            //var studenthandler = new ValidateStudent();
            //TMSStudentInfo studentinfo;
            //try
            //{
            //    studentinfo = studenthandler.GetValidStudent(studentid);
            //}
            //catch (Exception)
            //{
            //    Error = TMSErrorHandler.SetValidateStudentFailure(studentid, Errors.InvalidStudentID,
            //                                                  (int)Errors.InvalidStudentID, studentid);
            //    return false;
            //}
            //if (studentinfo == null)
            //{
            //    Error = TMSErrorHandler.SetValidateStudentFailure(studentid, Errors.InvalidStudentID,
            //                                                  (int)Errors.InvalidStudentID, studentid);
            //    return false;
            //}
            var studenthandler = new ValidateStudent();
            var studentinfo = studenthandler.GetValidStudentFromEx(studentid);
            if (string.IsNullOrEmpty(studentinfo.LastName))
            {
                Error = TMSErrorHandler.SetValidateStudentFailure(studentid, Errors.InvalidStudentID,
                                                                  (int) Errors.InvalidStudentID, studentid);
                return false;
            }
            return true;
        }
    }
}