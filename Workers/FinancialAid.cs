﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;
using System.Data;

namespace TMS.Workers
{
    public class FinancialAid : TMSWorkerBase
    {
        //private StudentFinancialAidAwards SFAAwards;
        private List<string> _exclude;
        private List<string> _include;
        private FAAwards final;
        private string term;

        public FAAwards GetFinancialAidAwards(string studentid, List<string> excludeItemIdArr, List<string> includeItemIdarr, string Term)
        {
            term = Term;

            final = new FAAwards { StudentId = studentid };
            GetFAAwards(studentid, excludeItemIdArr, includeItemIdarr);
            return final;
        }


        private void GetFAAwards(string studentid, List<string> excludeItemIdArr, List<string> includeItemIdarr)
        {
            _exclude = excludeItemIdArr;
            _include = includeItemIdarr;
            try
            {
                int returnValue = GetPendingAwards(studentid);

                switch (returnValue)
                {
                    case 1:
                        TMSErrorHandler.SetFinancialAidSuccess(ref final);
                        break;
                    case 0:
                        final = TMSErrorHandler.SetFinancialAidFailure(studentid, Errors.FinancialAidNoData, (int)Errors.FinancialAidNoData, studentid);
                        break;

                }
            }
            catch (Exception ex)
            {
                final = TMSErrorHandler.SetFinancialAidFailure(studentid, Errors.FinancialAidServerError, (int)Errors.FinancialAidServerError, ex.Message);
            }
        }

        private string IncludeExclude()
        {
            if (_exclude != null && _exclude.Count > 0)
            {
                var code = string.Empty;
                code = _exclude.Aggregate(code, (current, x) => current + ("'" + x.Trim() + "',"));
                code = code.Substring(0, code.Length - 1);
                return code;
            }

            if (_include == null || _include.Count <= 0) return string.Empty;


            var code2 = string.Empty;
            code2 = _include.Aggregate(code2, (current, x) => current + ("'" + x.Trim() + "',"));
            code2 = code2.Substring(0, code2.Length - 1);
            return code2;


        }

        private string GetCurrentTerm()
        {

            var at = new AvailableTerms();
            foreach (var i in at.GetTerms().Where(i => i.currentTermFlag))
            {
                return i.termCode;
            }

            var def = at.GetTerms().OrderBy(x => x.termStartDate).FirstOrDefault();
            return def != null ? def.termCode : "X_X";
        }

        private int GetPendingAwards(string studentid)
        {
            var exinclude = " AND TRANS_DESC {0}IN ({1})";
            if (_include.Count > 0 || _exclude.Count > 0)
            {
                exinclude = string.Format(exinclude, _exclude.Count > 0 ? "NOT " : "", IncludeExclude());
            }
            else
            {
                exinclude = string.Empty;
            }
            //var term = GetCurrentTerm();
            //var arrTerm = term.Split('_');
            string sql = string.Format("SELECT * FROM Trans_Hist where Source_Cde = '@F' AND ID_NUM = '{0}' {1}", studentid, exinclude);

            try
            {
                var conn = Connection();
                conn.Open();
                var command = new SqlCommand(sql, conn);
                var reader = command.ExecuteReader();
                var lst = new List<faEntryList>();
                bool count = false;

                while (reader.Read())
                {
                    count = true;
                    var item = new faEntryList();

                    item.id = ((int)reader["GROUP_NUM"]).ToString() + "-" + ((int)reader["TRANS_KEY_LINE_NUM"]).ToString();
                    item.amount = ((decimal)reader["TRANS_AMT"]).ToString();
                    item.description = reader["TRANS_DESC"].ToString();
                    item.studentId = ((int)reader["ID_NUM"]).ToString();
                    item.status = "Pending";
                    item.transactiondate = SafeConvertDateTime(reader["TRANS_DTE"].ToString());
                    item.termCode = reader["CHG_YR_TRAN_HIST"].ToString().Trim() + "_" + reader["CHG_TRM_TRAN_HIST"].ToString().Trim();
                    if (string.IsNullOrEmpty(term) || item.termCode == term)
                        lst.Add(item);
                }
                conn.Close();

                var distinctList = lst
                    .GroupBy(x => new { x.description, x.termCode })
                    .Select(g => g.First())
                    .ToList();

                foreach (var item in distinctList.Where(item => Convert.ToDecimal(item.amount) < 0))
                {
                    item.amount = (Convert.ToDecimal(item.amount) * -1).ToString();
                }
                final.EntryList = distinctList;
                return count ? 1 : 0;
            }
            catch (Exception ex)
            {
                final = TMSErrorHandler.SetFinancialAidFailure(studentid, Errors.FinancialAidServerError, (int)Errors.FinancialAidServerError, ex.Message);
            }
            return -1;
        }


        public void GetDataSet(ref DataSet ds, string SQL, string tablename)
        {
            SqlConnection conn = PfConnection();
            var da = new SqlDataAdapter();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = SQL;
            da.SelectCommand = cmd;

            conn.Open();
            da.Fill(ds, tablename);
            conn.Close();
        }

        public bool ValidateParameters(string studentid, ref FAAwards Error)
        {
            if (string.IsNullOrEmpty(studentid))
            {
                Error = TMSErrorHandler.SetFinancialAidFailure(studentid, Errors.NullStudentID,
                                              (int)Errors.NullStudentID, studentid);
                return false;
            }
            var studenthandler = new ValidateStudent();
            var studentinfo = studenthandler.GetValidStudentFromEx(studentid);
            if (string.IsNullOrEmpty(studentinfo.LastName))
            {
                Error = TMSErrorHandler.SetFinancialAidFailure(studentid, Errors.InvalidStudentID,
                                                                          (int)Errors.InvalidStudentID, studentid);
                return false;
            }
            return true;
        }

    }

}