﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers.Base;

namespace TMS.Workers
{
    public class FilterList : TMSWorkerBase
    {
        private List<FilterEntryList> _fel { get; set; }
        private getFilterList fl;
        private string _searchid { get; set; }
        private FilterListObject _filterlist { get; set; }
        private TMSMapping _mapping { get; set; }

        public getFilterList GetFilterList(string searchid, string filterlist)
        {
            if (_fel == null) _fel = new List<FilterEntryList>();
            if (IsMappingFilePresent()) GetMappingFile();
            else
            {
                return TMSErrorHandler.SetFilterListFailure(searchid, Errors.MissingConfigFile,
                                                            (int)Errors.MissingConfigFile, string.Empty);
            }
            
            _searchid = searchid;
            fl = new getFilterList();
            
            var lstarr = filterlist.Split(',');
            foreach (var innerarray in lstarr.Select(item => item.Split('|')))
            {

                if (innerarray[1].Substring(0, 2) == "F.")
                {
                    _filterlist = new FilterListObject
                                      {
                                          FilterName = innerarray[0],
                                          FilterTable = innerarray[1].Split('.')[1],
                                          FilterColumn = "val",
                                          FilterType = FilterListFilterType.Function
                                      };
                }
                else
                {
                    _filterlist = new FilterListObject
                                      {
                                          FilterName = innerarray[0],
                                          FilterTable = innerarray[1].Split('.')[0],
                                          FilterColumn = innerarray[1].Split('.')[1],
                                          FilterType = FilterListFilterType.Table
                                      };
                }
                BuildQuery();
            }
            if (fl.MessageNbr > 0) return fl;
            
            fl.FilterList = _fel;
            TMSErrorHandler.SetFilterListSuccess(searchid, ref fl);
            return fl;
        }

        private void BuildQuery()
        {
            if (_filterlist.FilterType != FilterListFilterType.Function && !TestDeterminatorColumn(_filterlist.FilterTable))
            {
                fl = TMSErrorHandler.SetFilterListFailure(_searchid, Errors.NoConfigForThisTable,
                                                            (int)Errors.NoConfigForThisTable, string.Empty);
                return;
            }
            var sb = new StringBuilder();
            if (_filterlist.FilterType == FilterListFilterType.Table)
            sb.AppendFormat("SELECT DISTINCT {0} FROM {1} WHERE {2} = '{3}'", _filterlist.FilterColumn, _filterlist.FilterTable,
                            GetDeterminatorColumn(_filterlist.FilterTable), _searchid);
            else
            {
                sb.AppendFormat("select dbo.{0}({1}) 'val'", _filterlist.FilterTable, _searchid);
            }

            


            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            
            
            try
            {
                var command = new SqlCommand(sb.ToString(), conn);
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var t = new FilterEntryList();
                    t.Name = _filterlist.FilterName;
                    t.Value = reader[_filterlist.FilterColumn].ToString();
                    _fel.Add(t);
                }
            }
            catch (Exception e)
            {
                fl = TMSErrorHandler.SetFilterListFailure(_searchid, Errors.GetFilterListError,
                                                            (int)Errors.GetFilterListError, e.Message.ToString());
                return;
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetDeterminatorColumn(string TableName)
        {
            foreach (var item in _mapping.Map.Where(item => item.Table == TableName))
            {
                return item.Column;
            }
            return string.Empty;
        }

        private bool TestDeterminatorColumn(string TableName)
        {
            foreach (var item in _mapping.Map.Where(item => item.Table == TableName))
            {
                return true;
            }
            return false;
        }

        private bool IsMappingFilePresent()
        {
            return File.Exists(GetMappingConfigFile());
        }

        private void GetMappingFile()
        {
            if (!File.Exists(GetMappingConfigFile())) return;
            var fsfile = new FileStream(GetMappingConfigFile(), FileMode.Open, FileAccess.Read);
            
            if (_mapping == null) _mapping = new TMSMapping();

            // Read the XML file.
            var file =
                new StreamReader(fsfile);
            var reader = new
                System.Xml.Serialization.XmlSerializer(_mapping.GetType());
            // Deserialize the content of the file into a Book object.
            _mapping = (TMSMapping)reader.Deserialize(file);
        }
    }

}