﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using Jenzabar.CRM;
using Jenzabar.CRM.Utility;
using Jenzabar.Common.Configuration;
using TMS.DataObjects;
using TMS.Helpers;

namespace TMS.Workers.Base
{
    public class TMSWorkerBase
    {
        protected string _authentication;
        protected readonly bool _isAuth;
        public const string appName = "ICSNET2"; // temp logic, hard-coded
        public enum FaultCode
        {

            Client = 0,

            Server = 1

        }

        private string debug { get; set; }

        protected string DebugInfo
        {
            get
            {
                return debug ?? string.Empty;
            }
            set { debug = value; }
        }


        public TMSWorkerBase()
        {
            //_authentication = authentication;
            _isAuth = true;
            _isAuth = new TMSAuthentication().IsAuthentic(_authentication);

        }

        //protected bool GetAccountDetails(string hostid, ref AccountDetailList accountDetails)
        //{
        //    var cleanid = SafeConvertInt(hostid).ToString();
        //    string strXML = "", strRecalcBalances = "";
        //    const string strCanViewPrelimData = "Y";

        //    try
        //    {
        //        var student = new Jenzabar.ERP.Student();
        //        student.GetAccountDetails(cleanid, strCanViewPrelimData, ref strRecalcBalances, ref strXML);
        //        accountDetails.AccountDetails = MapToAccountDetails(strXML);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw RaiseException("GetException", "WSSoapException", ex.Message,

        //          "1000", ex.Source, FaultCode.Server);

        //    }
        //}

        protected bool GetAccountDetails(string hostid, ref AccountDetailList accountDetail)
        {
            hostid = SafeConvertInt(hostid).ToString();
            var sql =string.Format(
                "SELECT th.Subsid_cde, th.TRANS_KEY_LINE_NUM, th.GROUP_NUM,  th.trans_desc AS TransWorkDesc, th.trans_dte AS WorkTransDte, th.trans_amt AS WorkTransAmt, th.subsid_trans_sts AS WorkTransSts, th.third_pty_bill_id AS WorkThirdPtyBillId, th.source_cde AS SourceCde, th.subsid_cde, th.CHG_YR_TRAN_HIST, th.CHG_TRM_TRAN_HIST, th.AR_CDE, th.CHG_FEE_CDE, SUBSID_DEF.SUBSID_DESC  FROM TRANS_HIST AS th LEFT OUTER JOIN SUBSID_DEF ON th.SUBSID_CDE = SUBSID_DEF.SUBSID_CDE AND th.SUBSID_CDE = SUBSID_DEF.SUBSID_CDE WHERE ( ( ( th.id_num = '{0}' AND th.offset_flag = 'R') AND ( th.subsid_trans_sts IN ('C', 'H', 'Y', 'U', 'S')) AND th.SOURCE_CDE != '@F')) ORDER BY th.trans_desc DESC", hostid);

            var ret = new List<AccountDetails>();
            var ad = new AccountDetails();
            var trans = new List<Transaction>();

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var t = new Transaction();
                t.Amount = SafeConvertDouble(reader["WorkTransAmt"].ToString());
                //t.DueDate = SafeConvertDateTime(reader["WorkTransDte"].ToString());
                t.TransactionDate = SafeConvertDateTime(reader["WorkTransDte"].ToString());
                t.ItemDescription = reader["TransWorkDesc"].ToString();
                t.ChargeStatus = reader["WorkTransSts"].ToString();
                t.ItemId = string.IsNullOrEmpty(reader["AR_CDE"].ToString()) ? reader["CHG_FEE_CDE"].ToString() : reader["AR_CDE"].ToString();
                t.TransactionType = t.Amount > 0 ? "Charge" : "Payment";
                t.TransactionId = string.Format("{0}.{1}.{2}", reader["SourceCde"], SafeConvertInt(reader["GROUP_NUM"].ToString()).ToString(), SafeConvertInt(reader["TRANS_KEY_LINE_NUM"].ToString()).ToString());
                t.TransactionTermID = GetTermCode(reader["CHG_YR_TRAN_HIST"].ToString(), reader["CHG_TRM_TRAN_HIST"].ToString());
                t.AR_Code = reader["AR_CDE"].ToString();
                t.ChargeFeeCode = reader["CHG_FEE_CDE"].ToString();
                t.SubsidiaryCode = reader["Subsid_cde"].ToString();
                t.SubsidiaryCodeDescription = reader["SUBSID_DESC"].ToString();
                trans.Add(t);
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }



            ad.Transactions = new Transaction[trans.Count];
            ad.Transactions = trans.ToArray();
            
            foreach (var ta in ad.Transactions)
            {
                ad.AccountBalance += ta.Amount;
            }
            ret.Add(ad);
            accountDetail.AccountDetails = ret;
            return true;

        }

        private string GetTermCode(string year, string term)
        {
            if (string.IsNullOrEmpty(year.Trim()) && string.IsNullOrEmpty(term.Trim())) return string.Empty;
            var ret = string.Format("{0}_{1}", year.Trim(), term.Trim());
            return ret == "_" ? string.Empty : ret;
        }

        protected bool LoopFixing()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSLoopFixing");
            return x != null && x == "true";
        }

        protected bool BlankReceiptNumber()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSBlankReceiptNumber");
            return x != null && x == "true";
        }

        protected bool UseDirectConnect()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSUseDirectConnect");
            return x != null && x == "true";
        }

        protected SqlConnection Connection()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSDatabaseString");
            return new SqlConnection(x);
        }

        protected SqlConnection PfConnection()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSPfDatabaseString");
            return new SqlConnection(x);
        }

        protected string GetTGCode()
        {

            var x = ConfigSettings.GetConfigValue("TMS", "TMSTransactionGroupCode");
            return string.IsNullOrEmpty(x) ? "TM" : x;
        }

        protected string GetMappingConfigFile()
        {

            var x = ConfigSettings.GetConfigValue("TMS", "TMSMappingConfig");
            return string.IsNullOrEmpty(x) ? "" : x;
        }

        protected bool UseSubsid()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSUseSubsidiaryCode");
            return x != null && x == "true";
        }

        protected bool UseSubsidForInclude()
        {
            var x = ConfigSettings.GetConfigValue("TMS", "TMSUseSubsidiaryCodeForInclude");
            return x != null && x == "true";
        }

        protected AccountCodes GetAccountCode(string arcode)
        {
            var sql = string.Format("SELECT [AR_CDE],[ACCT_CDE],[OFFSET_ACCT_CDE]FROM [AR_CODE] where AR_CDE = '{0}'", arcode);
            var ret = new AccountCodes();

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                ret.AccountCode = reader["ACCT_CDE"].ToString();
                ret.OffsetAccountCode = reader["OFFSET_ACCT_CDE"].ToString();
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        //private static List<AccountDetails> MapToAccountDetails(string inputxml)
        //{
        //    var ret = new List<AccountDetails>();
            
        //    var add = (CRMAccountDetails)PlugIn.MapXMLToObject(inputxml, new XmlSerializer(typeof(CRMAccountDetails)));

        //    if (add.Items.Length == 0) return null;

        //    foreach (CRMAccount t in add.Items)
        //    {
        //        var ad = new AccountDetails
        //                     {
        //                         AccountBalance = SafeConvertDouble(t.AccountBalance),
        //                         ID = t.AccountCode,
        //                         Message = t.AccountDesc
        //                     };

        //        if (t.AccountTransactions != null)
        //        {
        //            ad.Transactions = new Transaction[t.AccountTransactions.Length];
        //            for (var i = 0; i < t.AccountTransactions.Length; i++)
        //            {
        //                var tr = new Transaction
        //                             {
        //                                 Amount = SafeConvertDouble(t.AccountTransactions[i].TransactionAmount),
        //                                 ItemDescription = t.AccountTransactions[i].TransactionDescription,
        //                                 TransactionDate = SafeConvertDateTime(t.AccountTransactions[i].TransactionDate),

        //                             };
        //                if (t.AccountTransactions[i].TransactionMessage != null)
        //                    tr.ChargeStatus =
        //                        t.AccountTransactions[i].TransactionMessage[0].TransactionPending;
        //                if (!string.IsNullOrEmpty(t.AccountTransactions[i].TransactionDate)) tr.DueDate = SafeConvertDateTime(t.AccountTransactions[i].TransactionDate);
        //                ad.Transactions[i] = tr;
        //            }
        //        }
        //        ret.Add(ad);
        //    }
        //    return ret;

        //}

        protected static double SafeConvertDouble(string input)
        {
            try
            {
                return Convert.ToDouble(input);
            }
            catch (Exception)
            {
                return 0.00;
            }
        }

        protected static DateTime SafeConvertDateTime(string input)
        {
            try
            {
                return Convert.ToDateTime(input);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }
        protected static int SafeConvertInt(string input)
        {
            try
            {
                return Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public SoapException RaiseException(string uri, string webServiceNamespace,

                                 string errorMessage,

                                 string errorNumber,

                                 string errorSource,

                                 FaultCode code)
        {

            XmlQualifiedName faultCodeLocation = null;

            //Identify the location of the FaultCode

            switch (code)
            {

                case FaultCode.Client:

                    faultCodeLocation = SoapException.ClientFaultCode;

                    break;

                case FaultCode.Server:

                    faultCodeLocation = SoapException.ServerFaultCode;

                    break;

            }

            XmlDocument xmlDoc = new XmlDocument();

            //Create the Detail node

            XmlNode rootNode = xmlDoc.CreateNode(XmlNodeType.Element,

                                          SoapException.DetailElementName.Name,

                                          SoapException.DetailElementName.Namespace);

            //Build specific details for the SoapException

            //Add first child of detail XML element.

            XmlNode errorNode = xmlDoc.CreateNode(XmlNodeType.Element, "Error",

                                                  webServiceNamespace);

            //Create and set the value for the ErrorNumber node

            XmlNode errorNumberNode =

              xmlDoc.CreateNode(XmlNodeType.Element, "ErrorNumber",

                                webServiceNamespace);

            errorNumberNode.InnerText = errorNumber;

            //Create and set the value for the ErrorMessage node

            XmlNode errorMessageNode = xmlDoc.CreateNode(XmlNodeType.Element,

                                                        "ErrorMessage",

                                                        webServiceNamespace);

            errorMessageNode.InnerText = errorMessage;

            //Create and set the value for the ErrorSource node

            XmlNode errorSourceNode =

              xmlDoc.CreateNode(XmlNodeType.Element, "ErrorSource",

                                webServiceNamespace);

            errorSourceNode.InnerText = errorSource;

            //Append the Error child element nodes to the root detail node.

            errorNode.AppendChild(errorNumberNode);

            errorNode.AppendChild(errorMessageNode);

            errorNode.AppendChild(errorSourceNode);

            //Append the Detail node to the root node

            rootNode.AppendChild(errorNode);

            //Construct the exception

            SoapException soapEx = new SoapException(errorMessage,

                                                     faultCodeLocation, uri,

                                                     rootNode);

            //Raise the exception  back to the caller

            return soapEx;

        }


        public List<ErrorCodes> GetErrorCodeList()
        {
            return (from object e in Enum.GetValues(typeof (Errors)) select new ErrorCodes {Number = (int) e, Description = GetEnumDescription((Errors) e)}).ToList();
        }

        private string GetEnumDescription(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute),
                    false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        public List<String> TestDirectConnectionString()
        {
            var lst = new List<String>();
            //var sb = new StringBuilder();
            lst.Add("Connection String: " + ConfigSettings.GetConfigValue("TMS", "TMSDatabaseString"));
            lst.Add("Opening Connection");
            var conn = Connection();
            try { conn.Open();
            lst.Add("SUCCESS");}
            catch (Exception)
            {
                lst.Add("FAILURE");
            }
            lst.Add("Executing Query");
            try
            {
                var sql = "select TOP 1 * from TRANS_HIST";
                var command = new SqlCommand(sql, conn);
                var reader = command.ExecuteReader();
                var ids = new List<string>();

                while (reader.Read())
                {
                    ids.Add(((int) reader["TRANS_KEY_LINE_NUM"]).ToString());
                }
                if (ids.Count == 0)
                    lst.Add("NO DATA RETURNED");
                if (ids.Count == 1)
                    lst.Add("DATA RETURNED");
            }
            catch (Exception)
            {
                lst.Add("FAILURE");
            }
            lst.Add("Closing Connection");

            try
            {
                conn.Close();
                lst.Add("SUCCESS");
            }
            catch (Exception)
            {
                lst.Add("FAILURE");
            }

            return lst;

        }


    }
}