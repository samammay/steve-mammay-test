﻿using System;
using TMS.DataObjects;
using TMS.Helpers;

namespace TMS.Workers
{
    public class StatusChange : PaymentPlan
    {
        private StatusChangeReturn scr = new StatusChangeReturn();
        private string _sourcecode;
        private string _hostid;
        private string _subsidiarycode;
        private string _arcode;

        public StatusChangeReturn CreateStatusChange(string studentid, string changetype, string balance, string ARCode, string SubsidiaryCode, string Term, string Comment, string SourceCode)
        {
            _sourcecode = string.IsNullOrEmpty(SourceCode) ? GetTGCode() : SourceCode;
            _hostid = studentid;
            _subsidiarycode = SubsidiaryCode;
            _arcode = ARCode;
            switch (changetype)
            {
                case "Enrollment":
                    NewPlan(studentid, balance, ARCode, SubsidiaryCode, Term, Comment, _sourcecode);
                    break;
                case "Deactivation":
                    DeletePlan(studentid, Term);
                    break;
                case "Reactivation":
                    ReactivatePlan(studentid, balance, Term);
                    break;
                case "BudgetAdjustment":
                    ReactivatePlan(studentid, balance, Term);
                    break;
            }
            return scr;
        }

        private void NewPlan(string studentid, string balance, string ARCode, string SubsidiaryCode, string Term, string Comment, string SourceCode)
        {
            if (DoesPaymentPlanExist(studentid, Term))
            {
                scr = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.PaymentPlanAlreadyExists, (int)Errors.PaymentPlanAlreadyExists);
                return;
            }

            try
            {
                CreateNewPaymentPlan(studentid, balance, ARCode, SubsidiaryCode, Term, Comment, SourceCode);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                //scr = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.PaymentPlanErrorCreating, (int)Errors.PaymentPlanErrorCreating, ex.Message);
                return;
            }
            scr = TMSErrorHandler.SetStatusChangeSuccess(studentid);
        }

        private void HandleException(Exception ex)
        {

            //SourceCode
            if (ex.Message.IndexOf("BU_REF_8219") != -1 || ex.Message.IndexOf("BU_REF_7637") != -1)
            {
                scr = TMSErrorHandler.SetStatusChangeFailure(_hostid, Errors.GatewayPaymentInvalidSourceCode,
                                                      (int)Errors.GatewayPaymentInvalidSourceCode, _sourcecode);
                return;
            }

            //SubsidiaryCode BU_REF_11778
            if (ex.Message.IndexOf("BU_REF_11778") != -1)
            {
                scr = TMSErrorHandler.SetStatusChangeFailure(_hostid, Errors.GatewayPaymentInvalidSubsidiaryCode,
                                                      (int)Errors.GatewayPaymentInvalidSubsidiaryCode, _subsidiarycode);
                return;
            }

            //ARCode BU_REF_72639
            if (ex.Message.IndexOf("BU_REF_72639") != -1)
            {
                scr = TMSErrorHandler.SetStatusChangeFailure(_hostid, Errors.GatewayPaymentInvalidARCode,
                                                      (int)Errors.GatewayPaymentInvalidARCode, _arcode);
                return;
            }
            scr = TMSErrorHandler.SetStatusChangeFailure(_hostid, Errors.GatewayPaymentServerError, (int)Errors.GatewayPaymentServerError, ex.Message);

        }

        private void DeletePlan(string studentid, string Term)
        {
            if (!DoesPaymentPlanExist(studentid, Term))
            {
                scr = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.PaymentPlanMissing, (int)Errors.PaymentPlanMissing);
                return;
            }

            try
            {
                UpdatePaymentPlan(studentid, "0", Term);
                scr = TMSErrorHandler.SetStatusChangeSuccess(studentid);
            }
            catch (Exception ex)
            {
                scr = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.PaymentPlanDeletionError, (int)Errors.PaymentPlanDeletionError, ex.Message);
            }
        }

        private void ReactivatePlan(string studentid, string balance, string Term)
        {
            if (DoesPaymentPlanExist(studentid, Term))
            {
                try
                {
                    UpdatePaymentPlan(studentid, balance, Term);
                    scr = TMSErrorHandler.SetStatusChangeSuccess(studentid);
                    return;
                }
                catch (Exception ex)
                {
                    scr = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.PaymentPlanUpdateError, (int)Errors.PaymentPlanUpdateError, ex.Message);
                    return;
                }
            }

            scr = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.PaymentPlanPlanDoesntExist, (int)Errors.PaymentPlanPlanDoesntExist);
  
        }


        public bool ValidateParameters(string studentid, string ChangeType, string Balance, string LName, string ARCode,string SubsidiaryCode, string Term, ref StatusChangeReturn Error)
        {
            if (string.IsNullOrEmpty(studentid))
            {
                Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.NullStudentID,
                                              (int)Errors.NullStudentID, studentid);
                return false;
            }
            var studenthandler = new ValidateStudent();
            var studentinfo = studenthandler.GetValidStudentFromEx(studentid);
            if (string.IsNullOrEmpty(studentinfo.LastName))
            {
                Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.InvalidStudentID,
                                                                          (int)Errors.InvalidStudentID, studentid);
                return false;
            }
            if (string.IsNullOrEmpty(ChangeType))
            {
                Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.ChangeTypeCannotBeNull,
                                              (int)Errors.ChangeTypeCannotBeNull);
                return false;
            }

            if (ChangeType != "Enrollment" && ChangeType != "Deactivation" && ChangeType != "Reactivation" && ChangeType != "BudgetAdjustment")
            {
                Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.ChangeTypeCannotBeNull,
                                              (int)Errors.ChangeTypeCannotBeNull);
                return false;
            }

            var termcode = Term.Split('_');
            if (termcode.Length != 2)
            {
                Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.TermCodeInvalid,
                                              (int)Errors.TermCodeInvalid);
                return false;
            }

            if (ChangeType == "Enrollment")
            {
                if (string.IsNullOrEmpty(ARCode))
                {
                    Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.ARCodeCannotBeNull,
                                                  (int)Errors.ARCodeCannotBeNull);
                    return false;
                }
                if (string.IsNullOrEmpty(SubsidiaryCode))
                {
                    Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.SubsidiaryCodeCannotBeNull,
                                                  (int)Errors.SubsidiaryCodeCannotBeNull);
                    return false;
                }


}
            if (ChangeType != "Deactivation")
            {
                try
                {
                    Convert.ToDouble(Balance);
                }
                catch (Exception)
                {
                    Error = TMSErrorHandler.SetStatusChangeFailure(studentid, Errors.BalanceMustBePresentAndDouble,
                                                                   (int) Errors.BalanceMustBePresentAndDouble);
                    return false;
                }

            }


            if (string.IsNullOrEmpty(LName)) return true;

            if (studentinfo.LastName.Trim().ToLower() != LName.ToLower())
            {
                Error = TMSErrorHandler.SetStatusChangeMismatchFailure(studentid, Errors.StudentIDLNameMismatch,
                                                              (int)Errors.StudentIDLNameMismatch, studentinfo.LastName, LName);
                return false;
            }
            return true;
        }
    }
}