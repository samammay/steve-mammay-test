﻿Use ICS_NET
Go

IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSAuthenticationPw')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('741FD2D3-70EC-40D2-8715-BD1CB6B918D3', 'TMS', 'TMSAuthenticationPw', 'test', 'password')

	IF NOT EXISTS (SELECT * FROM COM_OnlinePaymentConfig WHERE [PaymentConfigDesc] = 'TMS Payment')
	INSERT INTO COM_OnlinePaymentConfig ([PaymentConfigID], [PaymentConfigDesc], [TransCode], [IsOnline], [IsCCOptionAvailable], [IsEcheckOptionAvailable]) 
	VALUES ('cb394abb-2799-40aa-b397-e1015ed0d6c0', 'TMS Payment', '099', 'true', 'true', 'false')

IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSDatabaseString')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('3e250b60-78b1-4b71-b32b-7bc902d975c9', 'TMS', 'TMSDatabaseString', 'Data Source=localhost;Initial Catalog=TmsEDmo;User ID=TMSUser;Password=r3susm7;Max Pool Size=200', 'Data Source=localhost;Initial Catalog=ICS_NET;User ID=ICSNetUser;Password=DCyhDeuA;Max Pool Size=200')


IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSPfDatabaseString')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('CFE98FAB-6E77-452C-B00F-46C7B8E46105', 'TMS', 'TMSPfDatabaseString', 'Data Source=localhost;Initial Catalog=PFAIDS_180;User ID=TMSUser;Password=r3susm7;Max Pool Size=200', 'Data Source=localhost;Initial Catalog=ICS_NET;User ID=ICSNetUser;Password=DCyhDeuA;Max Pool Size=200')


IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSBlankReceiptNumber')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('878b96d2-9cb3-4a68-940a-3fcd26ef7176', 'TMS', 'TMSBlankReceiptNumber', 'false', 'false')

IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSUseSubsidiaryCode')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('04f782eb-233a-43a7-a652-3a4396ea6346', 'TMS', 'TMSUseSubsidiaryCode', 'false', 'false')

IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSUseSubsidiaryCodeForInclude')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('fa95d5dc-b0f8-4099-8dd9-44883f7a6e00', 'TMS', 'TMSUseSubsidiaryCodeForInclude', 'false', 'false')

IF NOT EXISTS (SELECT * FROM FWK_ConfigSettings WHERE [Category] = 'TMS' AND [Key] = 'TMSMappingConfig')
	INSERT INTO FWK_ConfigSettings ([ID], [Category], [Key], [Value], [DefaultValue]) 
	VALUES ('354a7a3d-24b4-460a-a34e-1feb6438b929', 'TMS', 'TMSMappingConfig', 'C:\Program Files\Jenzabar\ICS.NET\Portal\ClientConfig\TMSMapping.xml', 'C:\Program Files\Jenzabar\ICS.NET\Portal\ClientConfig\TMSMapping.xml')


USE TmsEDmo
Go

	-- Create the TMSUser login if not there
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'TMSUser')
	-- adds the login setting the password policy off in case it doesn't meet the standards
	CREATE LOGIN TMSUser WITH password='r3susm7', check_policy=off 
GO

-- adds the login to the database
IF NOT EXISTS (SELECT * FROM dbo.sysusers where name = N'TMSUser' and uid < 16382)
	EXEC sp_grantdbaccess N'TMSUser', N'TMSUser'
GO

-- grant permissions to select from table
GRANT SELECT ON OBJECT::trans_hist TO TMSUser
Go

-- grant permissions to select from table
GRANT INSERT ON OBJECT::trans_hist TO TMSUser
Go

-- grant permissions to select from table
GRANT UPDATE ON OBJECT::trans_hist TO TMSUser
Go

-- grant permissions to select from table
GRANT SELECT ON OBJECT::year_def TO TMSUser
Go

-- grant permissions to select from table
GRANT SELECT ON OBJECT::year_term_table TO TMSUser
Go

-- grant permissions to select from table
GRANT SELECT ON OBJECT::term_def TO TMSUser
Go

-- grant permissions to select from table
GRANT SELECT ON OBJECT::SUBSID_DEF TO TMSUser
Go

-- grant permissions to select from table
GRANT SELECT ON OBJECT::NAME_MASTER TO TMSUser
Go

--GetStudentInfo Permissions
GRANT SELECT ON OBJECT::TD_PREFIX_VIEW TO TMSUser
Go

GRANT SELECT ON OBJECT::NAME_FORMAT_WEB_VIEW TO TMSUser
Go

GRANT SELECT ON OBJECT::TABLE_DETAIL TO TMSUser
Go

GRANT SELECT ON OBJECT::BIOGRAPH_MASTER TO TMSUser
Go

GRANT SELECT ON OBJECT::TD_STATE_VIEW TO TMSUser
Go

GRANT SELECT ON OBJECT::TD_COUNTRY_VIEW TO TMSUser
Go

GRANT SELECT ON OBJECT::ADDRESS_MASTER TO TMSUser
Go
--end GetStudentInfo Permissions

IF NOT EXISTS (SELECT * FROM SOURCE_MASTER WHERE [SOURCE_CDE] = 'TM')
	INSERT INTO [TmsEDmo].[dbo].[SOURCE_MASTER]
           ([SOURCE_CDE]
           ,[SOURCE_DESC]
	   ,[GRP_NUM_CTL]
           ,[ELIG_1098T]
           ,[BUDGET_CHECK_ON_TRANS_ENTRY]
           ,[USER_NAME])
     VALUES
           ('TM'
           ,'TMS Payment'
	   ,0
           ,'N'
           ,'N'
           ,'TMSUser')
GO

GRANT SELECT ON OBJECT::TRANS_BATCH_CTL TO TMSUser
Go
GRANT UPDATE ON OBJECT::TRANS_BATCH_CTL TO TMSUser
Go
GRANT INSERT ON OBJECT::TRANS_BATCH_CTL TO TMSUser
Go

GRANT SELECT ON OBJECT::source_master TO TMSUser
Go
GRANT UPDATE ON OBJECT::source_master TO TMSUser
Go

-- grant permissions to select from AR_CODE
GRANT SELECT ON OBJECT::AR_CODE TO TMSUser
Go