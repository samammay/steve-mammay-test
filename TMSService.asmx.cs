﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers;
using TMS.Workers.Base;

namespace TMS
{


    /// <summary>
    /// Summary description for TMS
    /// </summary>
    [WebService(Namespace = "http://TMS-JZabar.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class TMSService : SoapHttpClientProtocol
    {

        [SoapDocumentMethod("http://TMS-JZabar.org/TransactionDetails", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public AccountDetailList TransactionDetails(string StudentId, string includeItemId, string excludeItemId, string eligibleItemId, string ineligibleItemId, string Term)
        {
            var ab = new AccountBalance();

            var error = new AccountDetailList();
            if (!ab.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            var excludeItemIdarr = excludeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var eligibleItemIdarr = eligibleItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var includeItemIdarr = includeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var ineligibleItemIdarr = ineligibleItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();

            return ab.GetAccountBalance(StudentId, includeItemIdarr, excludeItemIdarr, eligibleItemIdarr, ineligibleItemIdarr, Term);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/ChargesAllowed", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        //[WebMethod]
        public string ChargesAllowed(string inputxml)
        {
            var ca = new ChargesAllowed();
            return ca.GetChargesAllowed(inputxml);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/GetErrorCodeList", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public List<ErrorCodes> GetErrorCodeList()
        {
            var b = new TMSWorkerBase();
            return b.GetErrorCodeList();

        }

        [SoapDocumentMethod("http://TMS-JZabar.org/AccountBalanceChanged", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public StudentDataList AccountBalanceChanged(string startDate, string endDate, string eligibleItemId, string ineligibleItemId)
        {
            var abc = new AccountBalanceChanged();
            var error = new StudentDataList();
            if (!new Gateway().ValidateParameters(startDate, endDate, eligibleItemId, ref error))
            {
                return error;
            }
            var strdate = Convert.ToDateTime(startDate);
            var enddate = Convert.ToDateTime(endDate);
            var elig = eligibleItemId != null ? eligibleItemId.Split(',') : new string[0];
            var inelig = ineligibleItemId != null ? ineligibleItemId.Split(',') : new string[0];
            return abc.GetAccountBalanceChanged(strdate, enddate, elig, inelig);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/AvailableTerms", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public TermData[] AvailableTerms()
        {
            var at = new AvailableTerms();
            return at.GetTerms();
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/getStudentInfo", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public TMSStudentInfo getStudentInfo(string StudentId)
        {
            var vs = new ValidateStudent();
            return vs.GetValidStudent(StudentId);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/AccountBalance", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public AccountDetailList AccountBalance(string StudentId, string includeItemId, string excludeItemId, string eligibleItemId)
        {
            var excludeItemIdarr = excludeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var eligibleItemIdarr = eligibleItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var includeItemIdarr = includeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();

            var ab = new AccountBalance();

            var error = new AccountDetailList();
            if (!ab.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            return ab.GetAccountBalanceOnly(StudentId, includeItemIdarr, excludeItemIdarr, eligibleItemIdarr);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/PostPayment", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public xmlReturn PostPayment(string StudentId, string LName, string PaymentAmount, string PaymentDate, string Source, string Comment, string SubsidiaryCode, string TmsPaymentId, string ARCode, string Term, string SourceCode)
        {
            var g = new Gateway();
            var error = new xmlReturn();
            if (
                !g.ValidateParameters(StudentId, LName, PaymentAmount, PaymentDate, Source, Comment, SubsidiaryCode,
                                      TmsPaymentId, ARCode, Term, ref error))
            {
                return error;
            }
            var pmtdte = Convert.ToDateTime(PaymentDate);
            var pmtamt = Convert.ToDouble(PaymentAmount);
            return g.PostPayment(StudentId, LName, pmtamt, pmtdte, Source, Comment, SubsidiaryCode, TmsPaymentId, ARCode, Term, SourceCode);
        }


        [SoapDocumentMethod("http://TMS-JZabar.org/PaymentPlanPayment", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public xmlPaymentPlanPayment PaymentPlanPayment(string StudentId, string LName, string PaymentAmount, string PaymentDate, string Source, string Comment, string SubsidiaryCode, string Balance, string TmsPaymentId, string ARCode, string Term, string SourceCode)
        {
            var p = new PaymentPlan();
            var error = new xmlPaymentPlanPayment();
            if (
                !p.ValidateParameters(StudentId, LName, PaymentAmount, PaymentDate, Source, Comment, SubsidiaryCode,
                                      TmsPaymentId, ARCode, Term, ref error))
            {
                return error;
            }

            var pmtdte = Convert.ToDateTime(PaymentDate);
            var pmtamt = Convert.ToDouble(PaymentAmount);
            return p.PostPaymentPlanPayment(StudentId, LName, pmtamt, pmtdte, Source, Comment, SubsidiaryCode, Balance, TmsPaymentId, ARCode, Term, SourceCode);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/StatusChange", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public StatusChangeReturn StatusChange(string ChangeType, string StudentId, string Balance, string LName, string Guid, string EndDate, string ARCode, string SubsidiaryCode, string Term, string Comment, string SourceCode)
        {
            var sc = new StatusChange();
            var error = new StatusChangeReturn();
            if (!sc.ValidateParameters(StudentId, ChangeType, Balance, LName, ARCode, SubsidiaryCode, Term, ref error))
            {
                return error;
            }
            return sc.CreateStatusChange(StudentId, ChangeType, Balance, ARCode, SubsidiaryCode, Term, Comment, SourceCode);
        }


        [SoapDocumentMethod("http://TMS-JZabar.org/FinancialAid", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public FAAwards FinancialAid(string StudentId, string includeItemId, string excludeItemId, string Term)
        {
            var fa = new FinancialAid();
            var excludeItemIdarr = excludeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var includeItemIdarr = includeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();

            var error = new FAAwards();
            if (!fa.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            return fa.GetFinancialAidAwards(StudentId, excludeItemIdarr, includeItemIdarr, Term);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/TestTerms", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        // [WebMethod]
        public FATerm[] TestTerms()
        {
            var at = new TermCodeConverter();
            return at.Test();
        }
        [SoapDocumentMethod("http://TMS-JZabar.org/TestDirectConnectionString", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public List<String> TestDirectConnectionString()
        {
            var baseclass = new TMSWorkerBase();
            return baseclass.TestDirectConnectionString();
        }


        [SoapDocumentMethod("http://TMS-JZabar.org/GetFilterList", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public getFilterList GetFilterList(string StudentId, string filterlist)
        {
            var fl = new FilterList();
            return fl.GetFilterList(StudentId, filterlist);
        }

        [SoapDocumentMethod("http://TMS-JZabar.org/GetActiveStudents", RequestNamespace = "http://TMS-JZabar.org/",
            ResponseNamespace = "http://TMS-JZabar.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [WebMethod]
        public ActiveStudents GetActiveStudents(string Term)
        {
            var activestudents = new ActiveStudentHandler();
            return activestudents.GetActiveStudents(Term);
        }
    }
}