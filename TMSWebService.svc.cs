﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMS.DataObjects;
using TMS.Helpers;
using TMS.Workers;
using TMS.Workers.Base;

namespace TMS
{
    
    public class TMSWebService : ITMSWebService
    {

        public TMSStudentInfo getStudentInfo(string StudentId)
        {
            var vs = new ValidateStudent();
            var error = new TMSStudentInfo();
            if (!vs.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            return vs.GetValidStudent(StudentId);
        }

        public AccountDetailList TransactionDetails(string StudentId, string includeItemId, string excludeItemId, string eligibleItemId, string ineligibleItemId, string Term)
        {
            
            var ab = new AccountBalance();

            var error = new AccountDetailList();
            if (!ab.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            var excludeItemIdarr = excludeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var eligibleItemIdarr = eligibleItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var includeItemIdarr = includeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var ineligibleItemIdarr = ineligibleItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();

            return ab.GetAccountBalance(StudentId, includeItemIdarr, excludeItemIdarr, eligibleItemIdarr, ineligibleItemIdarr, Term);
        }

        public List<ErrorCodes> GetErrorCodeList()
        {
            var b = new TMSWorkerBase();
            return b.GetErrorCodeList();

        }

        public StatusChangeReturn StatusChange(string ChangeType, string StudentId, string Balance, string LName, string Guid, string EndDate, string ARCode, string SubsidiaryCode, string Term, string Comment, string SourceCode)
        {
            var sc = new StatusChange();
            var error = new StatusChangeReturn();
            if (!sc.ValidateParameters(StudentId, ChangeType, Balance, LName, ARCode,SubsidiaryCode, Term,  ref error))
            {
                return error;
            }
            return sc.CreateStatusChange(StudentId, ChangeType, Balance, ARCode, SubsidiaryCode, Term, Comment, SourceCode);
        }

        public StudentDataList AccountBalanceChanged(string startDate, string endDate, string eligibleItemId, string ineligibleItemId)
        {
            var abc = new AccountBalanceChanged();
            var error = new StudentDataList();
            if (!new Gateway().ValidateParameters(startDate, endDate, eligibleItemId, ref error))
            {
                return error;
            }
            var strdate = Convert.ToDateTime(startDate);
            var enddate = Convert.ToDateTime(endDate);
            var elig = eligibleItemId != null ? eligibleItemId.Split(',') : new string[0];
            var inelig = ineligibleItemId != null ? ineligibleItemId.Split(',') : new string[0];
            return abc.GetAccountBalanceChanged(strdate, enddate, elig, inelig);
        }
        public TermData[] AvailableTerms()
        {
            var at = new AvailableTerms();
            return at.GetTerms();
        }


        public AccountDetailList AccountBalance(string StudentId, string includeItemId, string excludeItemId, string eligibleItemId)
        {
            var excludeItemIdarr = excludeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var eligibleItemIdarr = eligibleItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var includeItemIdarr = includeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();

            var ab = new AccountBalance();

            var error = new AccountDetailList();
            if (!ab.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            return ab.GetAccountBalanceOnly(StudentId, includeItemIdarr, excludeItemIdarr, eligibleItemIdarr);
        }


        public xmlReturn PostPayment(string StudentId, string LName, string PaymentAmount, string PaymentDate, string Source, string Comment, string SubsidiaryCode, string TmsPaymentId, string ARCode, string Term, string SourceCode)
        {
            var g = new Gateway();
            var error = new xmlReturn();
            if (
                !g.ValidateParameters(StudentId, LName, PaymentAmount, PaymentDate, Source, Comment, SubsidiaryCode,
                                      TmsPaymentId, ARCode, Term, ref error))
            {
                return error;
            }
            var pmtdte = Convert.ToDateTime(PaymentDate);
            var pmtamt = Convert.ToDouble(PaymentAmount);
            return g.PostPayment(StudentId, LName, pmtamt, pmtdte, Source, Comment, SubsidiaryCode, TmsPaymentId, ARCode, Term, SourceCode);
        }

        public xmlPaymentPlanPayment PaymentPlanPayment(string StudentId, string LName, string PaymentAmount, string PaymentDate, string Source, string Comment, string SubsidiaryCode, string Balance, string TmsPaymentId, string ARCode, string Term, string SourceCode)
        {
            var p = new PaymentPlan();
            var error = new xmlPaymentPlanPayment();
            if (
                !p.ValidateParameters(StudentId, LName, PaymentAmount, PaymentDate, Source, Comment, SubsidiaryCode,
                                      TmsPaymentId, ARCode, Term, ref error))
            {
                return error;
            }
            
            var pmtdte = Convert.ToDateTime(PaymentDate);
            var pmtamt = Convert.ToDouble(PaymentAmount);
            return p.PostPaymentPlanPayment(StudentId, LName, pmtamt, pmtdte, Source, Comment, SubsidiaryCode, Balance, TmsPaymentId, ARCode, Term, SourceCode);
        }


        public FAAwards FinancialAid(string StudentId, string includeItemId, string excludeItemId, string Term)
        {
            var fa = new FinancialAid();
            var excludeItemIdarr = excludeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();
            var includeItemIdarr = includeItemId.Split(',').ToList().Where(item => !string.IsNullOrEmpty(item)).ToList();

            var error = new FAAwards();
            if (!fa.ValidateParameters(StudentId, ref error))
            {
                return error;
            }
            return fa.GetFinancialAidAwards(StudentId, excludeItemIdarr, includeItemIdarr, Term);
        }

        public List<String> TestDirectConnectionString()
        {
            var baseclass = new TMSWorkerBase();
            return baseclass.TestDirectConnectionString();
        }
    }
}
