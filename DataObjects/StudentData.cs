﻿using System;
using System.Collections.Generic;

namespace TMS.DataObjects
{
    
    public class StudentData
    {
        public string ID;
        public string FirstName;
        public string LastName;
    }

    
    public class StudentDataList
    {
        private string pStatus = string.Empty;
        private Int32 pMessageNbr;
        private string pMessageText = string.Empty;
        public List<StudentData> StudentData { get; set; } 

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public Int32 MessageNbr
        {
            get { return pMessageNbr; }
            set { pMessageNbr = value; }
        }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }
    }
}