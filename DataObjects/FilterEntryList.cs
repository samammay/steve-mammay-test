﻿using System.Collections.Generic;
using TMS.DataObjects.Base;

namespace TMS.DataObjects
{
    public class FilterEntryList
    {

        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class FilterListObject
    {
        public string FilterName { get; set; }
        public string FilterTable { get; set; }
        public string FilterColumn { get; set; }
        public FilterListFilterType FilterType { get; set; }
    }

    public class getFilterList : DOBase
    {
        public List<FilterEntryList> FilterList { get; set; }
    }

    public enum FilterListFilterType
    {
        Table,
        Function,
        Procedure
    }
}