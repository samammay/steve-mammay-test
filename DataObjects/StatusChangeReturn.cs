﻿using System;

namespace TMS.DataObjects
{
    public class StatusChangeReturn 
    {
        private string pStatus = string.Empty;
        private string pStudentId = string.Empty;
        private Int32 pMessageNbr;
        private string pMessageText = string.Empty;
        public string StudentId
        {
            get { return pStudentId; }
            set { pStudentId = value; }
        }

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public Int32 MessageNbr
        {
            get { return pMessageNbr; }
            set { pMessageNbr = value; }
        }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }
    }
}