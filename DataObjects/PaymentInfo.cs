﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.DataObjects
{
    [Serializable]
    public class PaymentInfo
    {
        private decimal _amount;
        private string _amtstring;
        private string _description;
        private string _isappliedto;
        private string _method;
        private string _adminemail;
        private string _controlvalue;
        private Boolean _isonline;
        private string _transactionid;
        private string _splittenderid;
        protected List<PaymentInfoPayment> _payment = new List<PaymentInfoPayment>();

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public string AmtString
        {
            get { return _amtstring; }
            set { _amtstring = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string IsAppliedTo
        {
            get { return _isappliedto; }
            set { _isappliedto = value; }
        }
        public string Method
        {
            get { return _method; }
            set { _method = value; }
        }
        public string AdminEmail
        {
            get { return _adminemail; }
            set { _adminemail = value; }
        }
        public string ControlValue
        {
            get { return _controlvalue; }
            set { _controlvalue = value; }
        }
        public bool IsOnline
        {
            get { return _isonline; }
            set { _isonline = value; }
        }
        public string TransactionId
        {
            get { return _transactionid; }
            set { _transactionid = value; }
        }
        public string SplitTenderId
        {
            get { return _splittenderid; }
            set { _splittenderid = value; }
        }
        public List<PaymentInfoPayment> Payment
        {
            get { return _payment; }
            set { _payment = value; }
        }

        public int addECheck()
        {
            Payment.Add(new PaymentInfoPayment());
            int i = Payment.Count() - 1;
            Payment[i].BillingAddress = new PaymentInfoPaymentBillingAddress();
            Payment[i].ECheck = new PaymentInfoPaymentECheck();
            Payment[i].Response = new PaymentInfoPaymentResponse();
            return i;
        }
        public int addCreditCard()
        {
            Payment.Add(new PaymentInfoPayment());
            int i = Payment.Count() - 1;
            Payment[i].BillingAddress = new PaymentInfoPaymentBillingAddress();
            Payment[i].CreditCard = new PaymentInfoPaymentCreditCard();
            Payment[i].Response = new PaymentInfoPaymentResponse();
            return i;
        }

    }
    [Serializable]
    public class PaymentInfoPayment
    {
        private decimal _amount;
        private string _transactiondate;
        private PaymentInfoPaymentBillingAddress _billingaddress;
        private PaymentInfoPaymentCreditCard _creditcard;
        private PaymentInfoPaymentECheck _echeck;
        private PaymentInfoPaymentResponse _response;

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public string TransactionDate
        {
            get { return _transactiondate; }
            set { _transactiondate = value; }
        }
        public PaymentInfoPaymentBillingAddress BillingAddress
        {
            get { return _billingaddress; }
            set { _billingaddress = value; }
        }
        public PaymentInfoPaymentCreditCard CreditCard
        {
            get { return _creditcard; }
            set { _creditcard = value; }
        }
        public PaymentInfoPaymentECheck ECheck
        {
            get { return _echeck; }
            set { _echeck = value; }
        }
        public PaymentInfoPaymentResponse Response
        {
            get { return _response; }
            set { _response = value; }
        }
    }

    [Serializable]
    public class PaymentInfoPaymentBillingAddress
    {
        private string _firstname;
        private string _lastname;
        private string _companyname;
        private string _addrline;
        private string _city;
        private string _state;
        private string _zip;
        private string _country;
        private string _phone;
        private string _email;

        public string FirstName
        {
            get { return _firstname; }
            set { _firstname = value; }
        }
        public string LastName
        {
            get { return _lastname; }
            set { _lastname = value; }
        }
        public string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }
        public string AddrLine
        {
            get { return _addrline; }
            set { _addrline = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        public string EMail
        {
            get { return _email; }
            set { _email = value; }
        }

    }

    [Serializable]
    public class PaymentInfoPaymentCreditCard
    {
        private string _type;
        private string _number;
        private string _code;
        private string _exprdate;

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string ExprDate
        {
            get { return _exprdate; }
            set { _exprdate = value; }
        }
    }

    [Serializable]
    public class PaymentInfoPaymentECheck
    {
        private string _routing;
        private string _account;
        private string _type;
        private string _bank;
        private string _name;

        public string Routing
        {
            get { return _routing; }
            set { _routing = value; }
        }
        public string Account
        {
            get { return _account; }
            set { _account = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string Bank
        {
            get { return _bank; }
            set { _bank = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

    }

    [Serializable]
    public class PaymentInfoPaymentResponse
    {
        private string _responsecode;
        private string _responsetext;
        private string _subcode;
        private string _reasoncode;
        private string _authorizationcode;
        private string _transactionid;
        private string _erptransactionid;
        private string _balanceoncard;

        public string ResponseCode
        {
            get { return _responsecode; }
            set { _responsecode = value; }
        }
        public string ResponseText
        {
            get { return _responsetext; }
            set { _responsetext = value; }
        }
        public string SubCode
        {
            get { return _subcode; }
            set { _subcode = value; }
        }
        public string ReasonCode
        {
            get { return _reasoncode; }
            set { _reasoncode = value; }
        }
        public string AuthorizationCode
        {
            get { return _authorizationcode; }
            set { _authorizationcode = value; }
        }
        public string TransactionId
        {
            get { return _transactionid; }
            set { _transactionid = value; }
        }
        public string ERPTransactionId
        {
            get { return _erptransactionid; }
            set { _erptransactionid = value; }
        }
        public string BalanceOnCard
        {
            get { return _balanceoncard; }
            set { _balanceoncard = value; }
        }

    }
}