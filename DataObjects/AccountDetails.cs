﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;
using TMS.DataObjects.Base;

namespace TMS.DataObjects
{
    [GeneratedCode("wsdl", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://TMS-JZabar.org/")]
    public class AccountDetails
    {
        private double accountBalance;
        private string id;
        private string message;
        private CRMAccountPaymentPlan[] crmAccountPaymentPlans;

        private Transaction[] transactions;

        /// <remarks/>
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        /// <remarks/>
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <remarks/>
        public double AccountBalance
        {
            get { return accountBalance; }
            set { accountBalance = value; }
        }

        /// <remarks/>
        public Transaction[] Transactions
        {
            get { return transactions; }
            set { transactions = value; }
        }

        public CRMAccountPaymentPlan[] AccountPaymentPlan
        {
            get { return crmAccountPaymentPlans; }
            set { crmAccountPaymentPlans = value; }
        }
    }

    public class AccountDetailList 
    {
        private string pStatus = string.Empty;
        private string pStudentId = string.Empty;
        private Int32 pMessageNbr;
        private string pMessageText = string.Empty;
        public string StudentId
        {
            get { return pStudentId; }
            set { pStudentId = value; }
        }

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public Int32 MessageNbr
        {
            get { return pMessageNbr; }
            set { pMessageNbr = value; }
        }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }
        public List<AccountDetails> AccountDetails { get; set; }
    }
}