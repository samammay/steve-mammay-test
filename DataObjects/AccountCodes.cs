﻿namespace TMS.DataObjects
{
    public class AccountCodes
    {

        public string AccountCode { get; set; }
        public string OffsetAccountCode { get; set; }
    }
}