﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace TMS.DataObjects.Base
{

    [GeneratedCode("wsdl", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://TMS-JZabar.org/")]
    public class FinancialAidAwards
    {
        private string pMessageText = string.Empty;
        private string pStatus = string.Empty;
        public string StudentId { get; set; }

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public Int32 MessageNbr { get; set; }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }

        /// This is the comment for _studentname
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string StudentName { get; set; }

        /// This is the comment for _currentawardyearkey
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string CurrentAwardYearKey { get; set; }

        /// This is the comment for _availableawardyearlist
        [XmlArray(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("AwardYear", typeof (StudentFinancialAidAwardsAvailableAwardYearListAwardYear),
            Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAvailableAwardYearListAwardYear[] AvailableAwardYearList { get; set; }

        /// This is the comment for _awardlistheadings
        [XmlElement("AwardListHeadings", Form = XmlSchemaForm.Unqualified)]
        public StudentFinancialAidAwardsAwardListHeadings[] AwardListHeadings { get; set; }

        /// This is the comment for _aidawardlist
        [XmlArray(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("FinancialAidAward", typeof (StudentFinancialAidAwardsAidAwardListFinancialAidAward),
            Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAidAwardListFinancialAidAward[] AidAwardList { get; set; }
    }

    /// <remarks />
    public class StudentFinancialAidAwardsAvailableAwardYearListAwardYear
    {
        /// This is the comment for _awardyearsortorder
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardYearSortOrder { get; set; }

        /// This is the comment for _yearkey
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string YearKey { get; set; }

        /// This is the comment for _yeardesc
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string YearDesc { get; set; }
    }

    /// <remarks />
    public class StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation
    {
        /// This is the comment for _termkey
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermKey { get; set; }

        /// This is the comment for _termamount
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermAmount { get; set; }

        /// This is the comment for _termstatus
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermStatus { get; set; }

        /// This is the comment for _termstatusbubbletext
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermStatusBubbleText { get; set; }
    }

    /// <remarks />
    public class StudentFinancialAidAwardsAidAwardListFinancialAidAward
    {
        /// This is the comment for _erpstudentaidkey
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string ERPStudentAidKey { get; set; }

        /// This is the comment for _awarddescription
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardDescription { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardDescriptionShortname { get; set; }

        /// This is the comment for _awardamount
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardAmount { get; set; }

        /// This is the comment for _awardstatus
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardStatus { get; set; }

        /// This is the comment for _awardstatusbubbletext
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardStatusBubbleText { get; set; }

        /// This is the comment for _enableacceptdecline
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string EnableAcceptDecline { get; set; }

        /// This is the comment for _acceptdeclineresponse
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AcceptDeclineResponse { get; set; }

        /// This is the comment for _awardallocation
        [XmlArray(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("TermAllocation",
            typeof (StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation),
            Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation[] AwardAllocation { get; set; }
    }

    /// <remarks />
    public class StudentFinancialAidAwardsAwardListHeadingsAwardAllocationHeadingsTermHeaders
    {
        /// This is the comment for _termkey
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermKey { get; set; }

        /// This is the comment for _termamountheader
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermAmountHeader { get; set; }

        /// This is the comment for _termstatusheader
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TermStatusHeader { get; set; }
    }

    /// <remarks />
    public class StudentFinancialAidAwardsAwardListHeadings
    {
        /// This is the comment for _awarddescriptionheader
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardDescriptionHeader { get; set; }

        /// This is the comment for _awardamountheader
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardAmountHeader { get; set; }

        /// This is the comment for _awardstatusheader
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AwardStatusHeader { get; set; }

        /// This is the comment for _awardallocationheadings
        [XmlArray(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("TermHeaders",
            typeof (StudentFinancialAidAwardsAwardListHeadingsAwardAllocationHeadingsTermHeaders),
            Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAwardListHeadingsAwardAllocationHeadingsTermHeaders[] AwardAllocationHeadings { get; set; }
    }
}