﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace TMS.DataObjects
{
    /// <remarks/>
    [GeneratedCode("wsdl", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://TMS-JZabar.org/")]
    public class TermData
    {
        private bool currentTermFlagField;
        private string termCodeField;

        private string termDescField;

        private DateTime termEndDateField;
        private DateTime termStartDateField;

        /// <remarks/>
        public string termCode
        {
            get { return termCodeField; }
            set { termCodeField = value; }
        }

        /// <remarks/>
        public string termDesc
        {
            get { return termDescField; }
            set { termDescField = value; }
        }

        /// <remarks/>
        public DateTime termStartDate
        {
            get { return termStartDateField; }
            set { termStartDateField = value; }
        }

        /// <remarks/>
        public DateTime termEndDate
        {
            get { return termEndDateField; }
            set { termEndDateField = value; }
        }

        /// <remarks/>
        public bool currentTermFlag
        {
            get { return currentTermFlagField; }
            set { currentTermFlagField = value; }
        }
    }
}