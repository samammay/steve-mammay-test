﻿using System;
using TMS.DataObjects.Base;

namespace TMS.DataObjects
{
    public class xmlReturn 
    {
        private string pChargeId = string.Empty;
        private string pDepositId = string.Empty;
        private string pPaymentId = string.Empty;
        private string pStatus = string.Empty;
        private string pStudentId = string.Empty;
        private Int32 pMessageNbr;
        private string pMessageText = string.Empty;

        public xmlReturn()
        {
        }

        public xmlReturn(string cStudentId, string cStatus, Int32 cMessageNbr, string cMessageText, string cDepositId,
                         string cPaymentId, string cChargeId)
        {
            StudentId = cStudentId;
            Status = cStatus;
            MessageNbr = cMessageNbr;
            MessageText = cMessageText;
            DepositId = cDepositId;
            PaymentId = cPaymentId;
            ChargeId = cChargeId;
        }

        public string TransactionGroup { get; set; }
        public DateTime TransGroupCreationDate { get; set; }
        public string Cashier { get; set; }

        public string DepositId
        {
            get { return pDepositId; }
            set { pDepositId = value; }
        }

        public string PaymentId
        {
            get { return pPaymentId; }
            set { pPaymentId = value; }
        }

        public string ChargeId
        {
            get { return pChargeId; }
            set { pChargeId = value; }
        }




        public string StudentId
        {
            get { return pStudentId; }
            set { pStudentId = value; }
        }

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public Int32 MessageNbr
        {
            get { return pMessageNbr; }
            set { pMessageNbr = value; }
        }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }

    }
}