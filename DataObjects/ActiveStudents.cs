﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using TMS.DataObjects.Base;

namespace TMS.DataObjects
{
    public class ActiveStudents : DOBase
    {
        [XmlArray("ActiveStudentIds")]
        [XmlArrayItem("StudentId")]
        public List<String> ActiveStudentIds { get; set; } 
    }

}