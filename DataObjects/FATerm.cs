﻿using System;

namespace TMS.DataObjects
{
    public class FATerm
    {

        public int POCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TermCode { get; set; }
    }

    public class EXTerm
    {

        public int Year { get; set; }
        public int Term { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TermCode { get; set; }
    }
}