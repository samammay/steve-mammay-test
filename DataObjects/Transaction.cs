﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace TMS.DataObjects
{
    /// <remarks/>
    [GeneratedCode("wsdl", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://TMS-JZabar.org/")]
    public class Transaction
    {
        private double amount;

        private string chargeStatus;
       // private string comment;
        //private DateTime dueDate;
        private string itemDescription;
        private string itemId;

        private string notes;

        private string paymentPlanEligible;
        //private string postStatus;
        private DateTime transactionDate;
        private string transactionId;

        private string transactionTermID;
        private string transactionType;

        private string ARCode;
        private string chargefeecode;
        private string subsidiaryCode;
       // private double unpaidBalance;
        private string subsidiaryCodeDescription;

        /// <remarks/>
        public string TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }

        /// <remarks/>
        public string TransactionId
        {
            get { return transactionId; }
            set { transactionId = value; }
        }

        /// <remarks/>
        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }

        /// <remarks/>
        //public DateTime DueDate
        //{
        //    get { return dueDate; }
        //    set { dueDate = value; }
        //}

        /// <remarks/>
        public string ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }

        /// <remarks/>
        public string ItemDescription
        {
            get { return itemDescription; }
            set { itemDescription = value; }
        }

        /// <remarks/>
        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        /// <remarks/>
        //public double UnpaidBalance
        //{
        //    get { return unpaidBalance; }
        //    set { unpaidBalance = value; }
        //}

        /// <remarks/>
        public string SubsidiaryCodeDescription
        {
            get { return subsidiaryCodeDescription; }
            set { subsidiaryCodeDescription = value; }
        }

        /// <remarks/>
        public string ChargeStatus
        {
            get { return chargeStatus; }
            set { chargeStatus = value; }
        }

        /// <remarks/>
        //public string PostStatus
        //{
        //    get { return postStatus; }
        //    set { postStatus = value; }
        //}

        /// <remarks/>
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        /// <remarks/>
        public string PaymentPlanEligible
        {
            get { return paymentPlanEligible; }
            set { paymentPlanEligible = value; }
        }

        /// <remarks/>
        public string TransactionTermID
        {
            get { return transactionTermID; }
            set { transactionTermID = value; }
        }

        public string AR_Code
        {
            get { return ARCode; }
            set { ARCode = value; }
        }

        public string ChargeFeeCode
        {
            get { return chargefeecode; }
            set { chargefeecode = value; }
        }

        public string SubsidiaryCode
        {
            get { return subsidiaryCode; }
            set { subsidiaryCode = value; }
        }

    }
}