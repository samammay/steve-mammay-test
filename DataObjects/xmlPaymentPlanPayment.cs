﻿using System;

namespace TMS.DataObjects
{
    public class xmlPaymentPlanPayment
    {
        private string pChargeId = string.Empty;
        private string pDepositId = string.Empty;
        private string pMessageText = string.Empty;
        private string pPaymentId = string.Empty;
        private string pStatus = string.Empty;
        private string pStudentId = string.Empty;
        private string pCreditId = string.Empty;

        public xmlPaymentPlanPayment()
        {
        }

        public xmlPaymentPlanPayment(string cStudentId, string cStatus, Int32 cMessageNbr, string cMessageText, string cDepositId,
                 string cPaymentId, string cChargeId, string cCreditId)
        {
            StudentId = cStudentId;
            Status = cStatus;
            MessageNbr = cMessageNbr;
            MessageText = cMessageText;
            DepositId = cDepositId;
            PaymentId = cPaymentId;
            ChargeId = cChargeId;
            CreditId = cCreditId;

        }

        public string TransactionGroup { get; set; }
        public DateTime TransGroupCreationDate { get; set; }
        public string Cashier { get; set; }

        public string CreditId
        {
            get { return pCreditId; }
            set { pCreditId = value; }
        }

        public string StudentId
        {
            get { return pStudentId; }
            set { pStudentId = value; }
        }

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public int MessageNbr { get; set; }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }

        public string DepositId
        {
            get { return pDepositId; }
            set { pDepositId = value; }
        }

        public string PaymentId
        {
            get { return pPaymentId; }
            set { pPaymentId = value; }
        }

        public string ChargeId
        {
            get { return pChargeId; }
            set { pChargeId = value; }
        }
    }
}