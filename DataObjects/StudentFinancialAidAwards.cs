﻿using System;
using System.Xml.Serialization;

namespace TMS.DataObjects
{
    /// <remarks/>
    // XML from the  plug-in is used to populate this class.

    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class StudentFinancialAidAwards
    {

        public string StudentId { get; set; }

        public string Status { get; set; }

        public Int32 MessageNbr { get; set; }

        public string MessageText { get; set; }

        /// <remarks/>
        private string _studentname;

        /// <remarks/>
        private string _currentawardyearkey;

        /// <remarks/>
        private string _enableacceptbutton;

        /// <remarks/>
        private StudentFinancialAidAwardsAvailableAwardYearListAwardYear[] _availableawardyearlist;
        
        /// <remarks/>
        private StudentFinancialAidAwardsAidAwardListFinancialAidAward[] _aidawardlist;

        /// This is the comment for _studentname
        [XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string StudentName
        {
            get { return _studentname; }
            set { _studentname = value; }
        }

        /// This is the comment for _currentawardyearkey
        [XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CurrentAwardYearKey
        {
            get { return _currentawardyearkey; }
            set { _currentawardyearkey = value; }
        }
        
        /// This is the comment for _availableawardyearlist
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [XmlArrayItem("AwardYear", typeof (StudentFinancialAidAwardsAvailableAwardYearListAwardYear),
            Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAvailableAwardYearListAwardYear[] AvailableAwardYearList
        {
            get { return _availableawardyearlist; }
            set { _availableawardyearlist = value; }
        }

        /// This is the comment for _aidawardlist
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [XmlArrayItem("FinancialAidAward", typeof (StudentFinancialAidAwardsAidAwardListFinancialAidAward),
            Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAidAwardListFinancialAidAward[] AidAwardList
        {
            get { return _aidawardlist; }
            set { _aidawardlist = value; }
        }
    }

    /// <remarks/>
    public class StudentFinancialAidAwardsAvailableAwardYearListAwardYear
    {

        /// <remarks/>
        private string _awardyearsortorder;

        /// <remarks/>
        private string _yearkey;

        /// <remarks/>
        private string _yeardesc;

        /// This is the comment for _awardyearsortorder
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string AwardYearSortOrder
        {
            get { return _awardyearsortorder; }
            set { _awardyearsortorder = value; }
        }

        /// This is the comment for _yearkey
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string YearKey
        {
            get { return _yearkey; }
            set { _yearkey = value; }
        }

        /// This is the comment for _yeardesc
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string YearDesc
        {
            get { return _yeardesc; }
            set { _yeardesc = value; }
        }
    }

    /// <remarks/>
    public class StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation
    {

        /// <remarks/>
        private string _termkey;

        /// <remarks/>
        private string _termamount;

        /// <remarks/>
        private string _termstatus;

        /// This is the comment for _termkey
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TermKey
        {
            get { return _termkey; }
            set { _termkey = value; }
        }

        /// This is the comment for _termamount
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TermAmount
        {
            get { return _termamount; }
            set { _termamount = value; }
        }

        /// This is the comment for _termstatus
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TermStatus
        {
            get { return _termstatus; }
            set { _termstatus = value; }
        }
    }

    /// <remarks/>
    public class StudentFinancialAidAwardsAidAwardListFinancialAidAward
    {

        /// <remarks/>
        private string _erpstudentaidkey;

        /// <remarks/>
        private string _awarddescription;

        /// <remarks/>
        private string _awardamount;

        /// <remarks/>
        private string _awardstatus;

        /// <remarks/>
        private string _acceptdeclineresponse;

        /// <remarks/>
        private string _awarddescriptionshortname;

        /// <remarks/>
        private StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation[] _awardallocation;

        /// This is the comment for _erpstudentaidkey
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ERPStudentAidKey
        {
            get { return _erpstudentaidkey; }
            set { _erpstudentaidkey = value; }
        }

        /// This is the comment for _awarddescription
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string AwardDescription
        {
            get { return _awarddescription; }
            set { _awarddescription = value; }
        }

        /// This is the comment for _awarddescription
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string AwardDescriptionShortname
        {
            get { return _awarddescriptionshortname; }
            set { _awarddescriptionshortname = value; }
        }

        /// This is the comment for _awardamount
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string AwardAmount
        {
            get { return _awardamount; }
            set { _awardamount = value; }
        }

        /// This is the comment for _awardstatus
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string AwardStatus
        {
            get { return _awardstatus; }
            set { _awardstatus = value; }
        }

        /// This is the comment for _acceptdeclineresponse
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string AcceptDeclineResponse
        {
            get { return _acceptdeclineresponse; }
            set { _acceptdeclineresponse = value; }
        }

        /// This is the comment for _awardallocation
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [XmlArrayItem("TermAllocation",
            typeof (StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation),
            Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public StudentFinancialAidAwardsAidAwardListFinancialAidAwardAwardAllocationTermAllocation[] AwardAllocation
        {
            get { return _awardallocation; }
            set { _awardallocation = value; }
        }
    }

}