﻿using System;
using System.Collections.Generic;
using TMS.DataObjects.Base;

namespace TMS.DataObjects
{

    public class FAAwards
    {
        public string StudentId { get; set; }

        public string Status { get; set; }

        public Int32 MessageNbr { get; set; }

        public string MessageText { get; set; }
        public List<faEntryList> EntryList { get; set; }
    }
    public class faEntryList
    {

       public string termCode { get; set; }
        public string amount { get; set; }
        public string description { get; set; }
        public string id { get; set; }
        public string status { get; set; }
        public string studentId { get; set; }
        public DateTime transactiondate { get; set; }
    }

}

