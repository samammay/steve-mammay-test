﻿using System;

namespace TMS.DataObjects
{

    public class TMSStudentInfo
    {

        private string messageField;

        private string idField;

        private string firstNameField;

        private string middleNameField;

        private string lastNameField;

        private string titleField;

        private string suffixField;

        private string birthCountryField;

        private string birthStateField;

        private string birthCityField;

        private string addresseeField;

        private string salutationField;

        private string nickNameField;

        private string genderField;

        private string nameForDisplayField;

        private string birthDateField;

        private Address[] addressesField;

        private Contact[] contactsField;

        /// <remarks/>
        public string Message
        {
            get
            {
                return messageField;
            }
            set
            {
                messageField = value;
            }
        }

        /// <remarks/>
        public string ID
        {
            get
            {
                return idField;
            }
            set
            {
                idField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return firstNameField;
            }
            set
            {
                firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return middleNameField;
            }
            set
            {
                middleNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return lastNameField;
            }
            set
            {
                lastNameField = value;
            }
        }

        /// <remarks/>
        public string Title
        {
            get
            {
                return titleField;
            }
            set
            {
                titleField = value;
            }
        }

        /// <remarks/>
        public string Suffix
        {
            get
            {
                return suffixField;
            }
            set
            {
                suffixField = value;
            }
        }

        /// <remarks/>
        public string BirthCountry
        {
            get
            {
                return birthCountryField;
            }
            set
            {
                birthCountryField = value;
            }
        }

        /// <remarks/>
        public string BirthState
        {
            get
            {
                return birthStateField;
            }
            set
            {
                birthStateField = value;
            }
        }

        /// <remarks/>
        public string BirthCity
        {
            get
            {
                return birthCityField;
            }
            set
            {
                birthCityField = value;
            }
        }

        /// <remarks/>
        public string Addressee
        {
            get
            {
                return addresseeField;
            }
            set
            {
                addresseeField = value;
            }
        }

        /// <remarks/>
        public string Salutation
        {
            get
            {
                return salutationField;
            }
            set
            {
                salutationField = value;
            }
        }

        /// <remarks/>
        public string NickName
        {
            get
            {
                return nickNameField;
            }
            set
            {
                nickNameField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return genderField;
            }
            set
            {
                genderField = value;
            }
        }

        /// <remarks/>
        public string NameForDisplay
        {
            get
            {
                return nameForDisplayField;
            }
            set
            {
                nameForDisplayField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return birthDateField;
            }
            set
            {
                birthDateField = value;
            }
        }

        /// <remarks/>
        public Address[] Addresses
        {
            get
            {
                return addressesField;
            }
            set
            {
                addressesField = value;
            }
        }

        /// <remarks/>
        public Contact[] Contacts
        {
            get
            {
                return contactsField;
            }
            set
            {
                contactsField = value;
            }
        }

        private string pStatus = string.Empty;
        private string pStudentId = string.Empty;
        private Int32 pMessageNbr;
        private string pMessageText = string.Empty;
        public string StudentId
        {
            get { return pStudentId; }
            set { pStudentId = value; }
        }

        public string Status
        {
            get { return pStatus; }
            set { pStatus = value; }
        }

        public Int32 MessageNbr
        {
            get { return pMessageNbr; }
            set { pMessageNbr = value; }
        }

        public string MessageText
        {
            get { return pMessageText; }
            set { pMessageText = value; }
        }
    }


    public class Contact
    {

        private string contactTypeField;

        private string numEmailField;

        private string allowToShareField;

        /// <remarks/>
        public string ContactType
        {
            get
            {
                return contactTypeField;
            }
            set
            {
                contactTypeField = value;
            }
        }

        /// <remarks/>
        public string NumEmail
        {
            get
            {
                return numEmailField;
            }
            set
            {
                numEmailField = value;
            }
        }

        /// <remarks/>
        public string AllowToShare
        {
            get
            {
                return allowToShareField;
            }
            set
            {
                allowToShareField = value;
            }
        }
    }

    public class Address
    {

        private string typeField;

        private string isPrimaryAddressField;

        private string addressLine1Field;

        private string addressLine2Field;

        private string addressLine3Field;

        private string cityField;

        private string stateField;

        private string postCodeField;

        private string countryField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return typeField;
            }
            set
            {
                typeField = value;
            }
        }

        /// <remarks/>
        public string IsPrimaryAddress
        {
            get
            {
                return isPrimaryAddressField;
            }
            set
            {
                isPrimaryAddressField = value;
            }
        }

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return addressLine1Field;
            }
            set
            {
                addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return addressLine2Field;
            }
            set
            {
                addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine3
        {
            get
            {
                return addressLine3Field;
            }
            set
            {
                addressLine3Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return postCodeField;
            }
            set
            {
                postCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }
    }
}