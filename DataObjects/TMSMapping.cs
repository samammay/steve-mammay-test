﻿namespace TMS.DataObjects
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class TMSMapping
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Map")]
        public TMSMappingMap[] Map { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMSMappingMap
    {
        /// <remarks/>
        public string Table { get; set; }

        /// <remarks/>
        public string Column { get; set; }
    }


}