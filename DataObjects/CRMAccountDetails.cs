﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace TMS.DataObjects
{
    /// <remarks/>
    // XML from the EXStudent.MyAccountInfo plug-in is used to populate this class.
    [XmlRoot("AccountDetails", Namespace = "", IsNullable = false)]
    public class CRMAccountDetails
    {
        /// This is the comment for _items
        [XmlArray("AccountIDNumber", Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("Account", typeof (CRMAccount), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public CRMAccount[] Items { get; set; }
    }

    /// <remarks/>
    [XmlRoot("Account")]
    public class CRMAccount
    {
        /// This is the comment for _accountcode
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountCode { get; set; }

        /// This is the comment for _accountdesc
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountDesc { get; set; }

        /// This is the comment for _accountbalance
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountBalance { get; set; }

        /// This is the comment for _accountpaymentplan
        [XmlElement("AccountPaymentPlan", Form = XmlSchemaForm.Unqualified)]
        public CRMAccountPaymentPlan[] AccountPaymentPlan { get; set; }

        /// This is the comment for _accounttransactions
        [XmlElement("AccountTransactions", Form = XmlSchemaForm.Unqualified)]
        public CRMAccountTransactions[] AccountTransactions { get; set; }
    }

    /// <remarks/>
    [XmlRoot("AccountPaymentPlan")]
    public class CRMAccountPaymentPlan
    {
        /// This is the comment for _accountpaymentplankey
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountPaymentPlanKey { get; set; }

        /// This is the comment for _accountpaymentplandesc
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountPaymentPlanDesc { get; set; }

        /// This is the comment for _accountpaymentplanamount
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountPaymentPlanAmount { get; set; }

        /// This is the comment for _accountpaymentplanduedate
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string AccountPaymentPlanDueDate { get; set; }
    }

    /// <remarks/>
    [XmlRoot("TransactionMessage")]
    public class CRMTransactionMessage
    {
        /// This is the comment for _transactionpending
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TransactionPending { get; set; }

        /// This is the comment for _transaction3rdparty
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string Transaction3rdParty { get; set; }
    }

    /// <remarks/>
    [XmlRoot("AccountTransactions")]
    public class CRMAccountTransactions
    {
        /// This is the comment for _transactiondescription
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TransactionDescription { get; set; }

        /// This is the comment for _transactiondate
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TransactionDate { get; set; }

        /// This is the comment for _transactionamount
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string TransactionAmount { get; set; }

        /// This is the comment for _transactionmessage
        [XmlElement("TransactionMessage", Form = XmlSchemaForm.Unqualified)]
        public CRMTransactionMessage[] TransactionMessage { get; set; }
    }
}