﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TMS.Helpers
{

    public class ErrorCodes
    {
        public int Number { get; set; }
        public string Description { get; set; }
    }

   
    public enum Errors
    {
        [Description("General Fault")]
        GeneralFault,
        [Description("Success")]
        Success,
        [Description("Payment Plan does not exist, therfore it cannot be deleted")]
        PaymentPlanMissing,
        [Description("There was an error trying to delete this payment plan")]
        PaymentPlanDeletionError,
        [Description("This student already has a payment plan in place")]
        PaymentPlanAlreadyExists,
        [Description("There was an error trying to create a new payment plan. {0}")]
        PaymentPlanErrorCreating,
        [Description("There was an error trying to update this payment plan")]
        PaymentPlanUpdateError,
        [Description("There was an error trying to reactivate this payment plan. It does not exist.")]
        PaymentPlanPlanDoesntExist,
        [Description("StudentId cannot be null")]
        NullStudentID,
        [Description("SubsidiaryCode cannot be null")]
        NullBillingItem,
        [Description("The amount sent as PaymentAmount is not a double")]
        PaymentAmountNotDouble,
        [Description("The payment date is invalid")]
        InvalidPaymentDate,
        [Description("StudentId: {0} is invalid")]
        InvalidStudentID,
        [Description("StudentId: {0} and LastName: {2} do not match last name of record: {1}")]
        StudentIDLNameMismatch,
        [Description("The start date is missing or invalid")]
        AccountBalanceChangedInvalidStartDate,
        [Description("The end date is missing or invalid")]
        AccountBalanceChangedInvalidEndDate,
        [Description("EligibleItemID is a required field")]
        AccountBalanceChangedMissingEligibleItemId,
        [Description("ChangeType is either null or an invalid type.  Possible types are Enrollment, Deactivation, Reactivation or BudgetAdjustment.")]
        ChangeTypeCannotBeNull,
        [Description("For an Enrollment, Reactivation or BudgetAdjustment Status Change, Balance must be present and a double type number")]
        BalanceMustBePresentAndDouble,
        [Description("For an Enrollment Status Change, ARCode must be submitted")]
        ARCodeCannotBeNull,
        [Description("A payment cannot be posted to this payment plan.  The payment plan does not exist.")]
        PaymentPlanDoesNotExist,
        [Description("For an Enrollment Status Change, SubsidiaryCode must be submitted")]
        SubsidiaryCodeCannotBeNull,
        [Description("There was an error returned by the server while posting a payment.  The text of the error is : {0}")]
        GatewayPaymentServerError,
        [Description("The term is missing or invalid")]
        TermCodeInvalid,
        [Description("There was no data returned for {0}")]
        FinancialAidNoData,
        [Description("There was an error returned by the server while getting financial aid information.  The text of the error is : {0}")]
        FinancialAidServerError,
        [Description("There was no year entered.  It is a manditory field.")]
        MissingYear,
        [Description("There was no PaymentId entered.  It is a manditory field.")]
        NullPaymentId,
        [Description("The file used to convert StudentID to the appropriate column is missing.")]
        MissingConfigFile,
        [Description("There is no configuration found for this table.")]
        NoConfigForThisTable,
        [Description("There was an error returned by the server while calling GetFilterList. The text of the error is : {0}.")]
        GetFilterListError,
        [Description("The given term, {0}, is not valid. Please resubmit with a valid term.")]
        InvalidTermForActiveStudents,
        [Description("The Source Code for this transaction is invalid:  SourceCode:{0}")]
        GatewayPaymentInvalidSourceCode,
        [Description("There was an error returned by GetStudentInfo:  Error text: {0}")]
        GetStudentInfoError,
        [Description("The Subsidiary Code for this transaction is invalid:  SubsidiaryCode:{0}")]
        GatewayPaymentInvalidSubsidiaryCode,
        [Description("The ARCode for this transaction is invalid:  ARCode:{0}")]
        GatewayPaymentInvalidARCode


    }

    public class Enumeration
    {
       public string GetEnumDescription(Enum value)
{
    var fi = value.GetType().GetField(value.ToString());

    var attributes =
        (DescriptionAttribute[])fi.GetCustomAttributes(
        typeof(DescriptionAttribute),
        false);

    return attributes.Length > 0 ? attributes[0].Description : value.ToString();
}  

    }

   

}