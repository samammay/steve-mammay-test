﻿using Jenzabar.Common.Configuration;

namespace TMS.Helpers
{
    public class TMSAuthentication
    {

        private const string TEXTKEY = "TMSAuthenticationPw";
        private const string CATEGORYKEY = "TMS";

        public bool IsAuthentic(string pw)
        {
            return Validate(pw);
        }
        
        private static bool Validate(string pw)
        {
           var key =  ConfigSettings.GetConfigValue(CATEGORYKEY, TEXTKEY);
           return !string.IsNullOrEmpty(key) && key == pw;
        } 
} 

}