﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using TMS.DataObjects;
using TMS.Workers;
using TMS.Workers.Base;

namespace TMS.Helpers
{
    public class TermCodeConverter : TMSWorkerBase
    {

        private List<FATerm> TermList { get; set; }
        private List<EXTerm> EXTerms { get; set; } 

        public string GetTermCodeFromPO(string POCode)
        {
            if (TermList == null) LoadTermList();
            foreach (var i in TermList.Where(i => i.POCode == Convert.ToInt32(POCode)))
            {
                return i.TermCode;
            }
            return string.Empty;
        }

        public FATerm[] Test()
        {
            if (TermList == null) LoadTermList();
            return TermList.ToArray();
        }

        private void LoadTermList()
        {
            TermList = new List<FATerm>();
            EXTerms = new List<EXTerm>();
            GetListOfPOCodes();
            GetListOfEXTerms();
            MergeLists();
        }

        private void GetListOfPOCodes()
        {

            const string sql = "SELECT [poe_token],[start_date],[end_date] FROM [poe]";

            var conn = PfConnection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var cde = new FATerm
                {
                    POCode = Convert.ToInt32(reader["poe_token"].ToString()),
                    StartDate = Convert.ToDateTime(reader["start_date"].ToString()),
                    EndDate = Convert.ToDateTime(reader["end_date"].ToString())
                };
                TermList.Add(cde);
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GetListOfEXTerms()
        {
            const string sql = "Select [YR_CDE] ,[TRM_CDE] ,[TRM_BEGIN_DTE],[TRM_END_DTE],[YR_TRM_DESC] FROM [YEAR_TERM_TABLE]";

            var conn = Connection();
            try { conn.Open(); }
            catch (Exception ex)
            {
                throw ex;
            }

            var command = new SqlCommand(sql, conn);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var cde = new EXTerm
                              {
                                  Year = Convert.ToInt32(reader["YR_CDE"].ToString()),
                                  Term = Convert.ToInt32(reader["TRM_CDE"].ToString()),
                                  StartDate = Convert.ToDateTime(reader["TRM_BEGIN_DTE"].ToString()),
                                  EndDate = Convert.ToDateTime(reader["TRM_END_DTE"].ToString())
                              };
                cde.TermCode = cde.Year + "_" + cde.Term;
                EXTerms.Add(cde);
            }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void MergeLists()
        {
            foreach (var fa in TermList)
            {
                fa.TermCode = GetTermCodeFromEX(fa.StartDate, fa.EndDate);
            }
        }

        private string GetTermCodeFromEX(DateTime startdate, DateTime enddate)
        {
            var working = startdate.AddDays(5);
            var possible = EXTerms.Where(d => working > d.StartDate && working < d.EndDate).ToList();
            if (possible.Count == 1) return possible[0].TermCode;
            if (possible.Count == 0) return string.Empty;
            var inx = new Dictionary<int, string>();
            foreach (EXTerm t in possible)
            {
                var a = startdate - t.StartDate;
                var b = enddate - t.EndDate;
                inx.Add(a.Days + b.Days, t.TermCode);
            }
            var min = inx.OrderBy(kvp => kvp.Key).First();
            return min.Value;
        }

        public bool PopulateDates(string Term, ref DateTime termStart, ref DateTime termEnd)
        {
            var terms = new AvailableTerms().GetTerms();
            foreach (var t in terms.Where(t => t.termCode == Term))
            {
                termStart = t.termStartDate;
                termEnd = t.termEndDate;
                return true;
            }
            return false;
        }
    }
}