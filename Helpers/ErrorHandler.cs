﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.DataObjects;

namespace TMS.Helpers
{
    public static class TMSErrorHandler
    {
        private const string errorresponse = "The server returned the following error message: {0}";

        public static Enumeration Enumeration = new Enumeration();
        public static StatusChangeReturn SetStatusChangeFailure(string studentid, Enum error, int errornumber)
        {
            return SetStatusChangeFailure(studentid, error, errornumber, string.Empty);
        }

        public static StatusChangeReturn SetStatusChangeFailure(string studentid, Enum error, int errornumber, string additionaltext)
        {
            string errormessage;
            if (!string.IsNullOrEmpty(additionaltext) && additionaltext != studentid)
                errormessage = string.Format(errorresponse, additionaltext);
            else errormessage = additionaltext;
            var ret = new StatusChangeReturn
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), errormessage),
                StudentId = studentid
            };
            return ret;
        }

        public static StatusChangeReturn SetStatusChangeSuccess(string studentid)
        {
            var ret = new StatusChangeReturn
            {
                Status = "Success",
                MessageNbr = 1,
                MessageText = "Status Change Successful",
                StudentId = studentid
            };
            return ret;
        }

        public static xmlReturn SetPostPaymentFailure(string studentid, Enum error, int errornumber)
        {
            return SetPostPaymentFailure(studentid, error, errornumber, string.Empty);
        }

        public static xmlReturn SetPostPaymentFailure(string studentid, Enum error, int errornumber, string additionaltext)
        {
            var ret = new xmlReturn
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), additionaltext),
                StudentId = studentid
            };
            return ret;
        }
        public static xmlReturn SetPostPaymentMismatchFailure(string studentid, Enum error, int errornumber, string additionaltext, string additionaltext2)
        {
            var ret = new xmlReturn
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), studentid, additionaltext, additionaltext2),
                StudentId = studentid
            };
            return ret;
        }

        public static void SetPostPaymentSuccess(string studentid, ref xmlReturn ret)
        {
            ret.Status = "Success";
            ret.MessageNbr = 1;
            ret.MessageText = "Post Payment Successful";
            ret.StudentId = studentid;

        }

        public static xmlPaymentPlanPayment SetPaymentPlanPaymentFailure(string studentid, Enum error, int errornumber)
        {
            return SetPaymentPlanPaymentFailure(studentid, error, errornumber, string.Empty);
        }

        public static xmlPaymentPlanPayment SetPaymentPlanPaymentFailure(string studentid, Enum error, int errornumber, string additionaltext)
        {
            var ret = new xmlPaymentPlanPayment
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), additionaltext),
                StudentId = studentid
            };
            return ret;
        }
        public static xmlPaymentPlanPayment SetPaymentPlanPaymentMismatchFailure(string studentid, Enum error, int errornumber, string additionaltext, string additionaltext2)
        {
            var ret = new xmlPaymentPlanPayment
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), studentid, additionaltext, additionaltext2),
                StudentId = studentid
            };
            return ret;
        }

        public static xmlPaymentPlanPayment SetPaymentPlanPaymentSuccess(string studentid)
        {
            var ret = new xmlPaymentPlanPayment
            {
                Status = "Success",
                MessageNbr = 1,
                MessageText = "Payment Plan Payment Successful",
                StudentId = studentid
            };
            return ret;
        }

        public static void SetAccountBalanceChangedSuccess(ref StudentDataList sdl)
        {
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "AccountBalanceChanged method call Successful";
        }
        public static StudentDataList SetAccountBalanceChangedFailure(Enum error, int errornumber)
        {
            var ret = new StudentDataList
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = Enumeration.GetEnumDescription(error)
            };
            return ret;
        }

        public static AccountDetailList SetAccountBalanceFailure(string studentid, Enum error, int errornumber)
        {
            return SetAccountBalanceFailure(studentid, error, errornumber, string.Empty);
        }
        public static AccountDetailList SetAccountBalanceFailure(string studentid, Enum error, int errornumber, string additionaltext)
        {
            var ret = new AccountDetailList
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), studentid, additionaltext),
                StudentId = studentid
            };
            return ret;
        }


        public static TMSStudentInfo SetValidateStudentFailure(string studentid, Enum error, int errornumber)
        {
            return SetValidateStudentFailure(studentid, error, errornumber, string.Empty);
        }

        public static TMSStudentInfo SetValidateStudentFailure(string studentid, Enum error, int errornumber, string additionaltext)
        {
            var ret = new TMSStudentInfo
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), studentid, additionaltext),
                StudentId = studentid
            };
            return ret;
        }

        public static StatusChangeReturn SetStatusChangeMismatchFailure(string studentid, Enum error, int errornumber, string additionaltext, string additionaltext2)
        {
            var ret = new StatusChangeReturn
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), studentid, additionaltext, additionaltext2),
                StudentId = studentid
            };
            return ret;
        }

        public static void SetFinancialAidSuccess(ref FAAwards sdl)
        {
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "FinancialAid method call Successful";
        }

        public static FAAwards SetFinancialAidFailure(string studentid, Enum error, int errornumber,
                                                                       string additionaltext)
        {
            var ret = new FAAwards
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), additionaltext),
                StudentId = studentid
            };
            return ret;
        }

        public static void SetTransactionsSuccess(string hostid, ref AccountDetailList sdl)
        {
            sdl.StudentId = hostid;
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "TransactionDetails method call Successful";
        }

        public static void SetFilterListSuccess(string hostid, ref getFilterList sdl)
        {
            sdl.StudentId = hostid;
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "GetFilterList method call Successful";
        }

        public static getFilterList SetFilterListFailure(string studentid, Enum error, int errornumber,
                                                               string additionaltext)
        {
            var ret = new getFilterList
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), additionaltext),
                StudentId = studentid
            };
            return ret;
        }

        public static ActiveStudents SetActiveStudentsFailure(Enum error, int errornumber,
                                                               string additionaltext)
        {
            var ret = new ActiveStudents
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), additionaltext)
            };
            return ret;
        }

        public static void SetActiveStudentSuccess(ref ActiveStudents sdl)
        {
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "GetActiveStudents method call Successful";
        }

        public static void SetActiveStudentSuccessWithNoActiveStudents(ref ActiveStudents sdl)
        {
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "GetActiveStudents method call Successful, however there are no active students for this term";
        }

        public static AccountDetailList SetTransactionsFailure(string studentid, Enum error, int errornumber,
                                                       string additionaltext)
        {
            var ret = new AccountDetailList
            {
                StudentId = studentid,
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), additionaltext)
            };
            return ret;
        }

        public static TMSStudentInfo SetGetStudentInfoFailure(string studentid, Enum error, int errornumber, string additionaltext)
        {
            string errormessage;
            if (!string.IsNullOrEmpty(additionaltext) && additionaltext != studentid)
                errormessage = string.Format(errorresponse, additionaltext);
            else errormessage = additionaltext;
            var ret = new TMSStudentInfo
            {
                Status = "Failure",
                MessageNbr = errornumber,
                MessageText = string.Format(Enumeration.GetEnumDescription(error), errormessage),
                StudentId = studentid
            };
            return ret;
        }

        public static void SetGetStudentInfoSuccess(ref TMSStudentInfo sdl)
        {
            sdl.Status = "Success";
            sdl.MessageNbr = 1;
            sdl.MessageText = "GetStudentInfo method call Successful";
        }
    }
}