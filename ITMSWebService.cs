﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TMS.DataObjects;
using TMS.Helpers;

namespace TMS
{
    [ServiceContract]
    public interface ITMSWebService
    {
        [OperationContract]
        TMSStudentInfo getStudentInfo(string StudentId);

        [OperationContract]
        AccountDetailList TransactionDetails(string StudentId, string includeItemId, string excludeItemId, string eligibleItemId, string ineligibleItemId, string Term);

        [OperationContract]
        List<ErrorCodes> GetErrorCodeList();
        //string ChargesAllowed(string inputxml);

        [OperationContract]
        StudentDataList AccountBalanceChanged(string startDate, string endDate, string eligibleItemId, string ineligibleItemId);

        [OperationContract] 
        TermData[] AvailableTerms();

        [OperationContract] 
        AccountDetailList AccountBalance(string StudentId, string includeItemId, string excludeItemId,
                                                   string eligibleItemId);

        [OperationContract] 
        xmlReturn PostPayment(string StudentId, string LName, string PaymentAmount, string PaymentDate,
                                     string Source, string Comment, string SubsidiaryCode, string TmsPaymentId,
                                     string ARCode, string Term, string SourceCode);

        [OperationContract] 
        xmlPaymentPlanPayment PaymentPlanPayment(string StudentId, string LName, string PaymentAmount,
                                                        string PaymentDate, string Source, string Comment,
                                                        string SubsidiaryCode, string Balance, string TmsPaymentId,
                                                        string ARCode, string Term, string SourceCode);

        [OperationContract]
        StatusChangeReturn StatusChange(string ChangeType, string StudentId, string Balance, string LName, string Guid, string EndDate, string ARCode, string SubsidiaryCode, string Term, string Comment, string SourceCode);


        [OperationContract]
        FAAwards FinancialAid(string StudentId, string includeItemId, string excludeItemId, string Term);

        [OperationContract]
        List<String> TestDirectConnectionString();

    }
}
